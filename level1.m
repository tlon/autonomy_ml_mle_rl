% Level 1: parameter estimation

%% 34methylene
clear; close all; clc;
set(groot,'defaultLineLineWidth', 1.5)
set(groot,'defaultAxesFontSize', 16)
set(groot,'defaulttextinterpreter','latex');  
set(groot, 'defaultAxesTickLabelInterpreter','latex');  
set(groot, 'defaultLegendInterpreter','latex');
p = gcp;

%% Loads

load('environment_SE_spacecraft_cubesat.mat');
load('connection.mat');
%% Setup

c = 299792/normalizers.vel_norm; % CR3BP normalized speed of light
SRP_flux_dim = 1357; % [W/m^2] = [kgm^2/s^3/m^2] = [kg/s^3]
flux_normalizer = normalizers.m_norm/normalizers.time_norm^3;
SRP_flux = SRP_flux_dim/flux_normalizer;
A_sc = 0.01*0.02/LU^2;

%%

C_R_avg = 1.5;
C_R_sigma = 0.1;

tof = connection.connection_ms.total_time - (connection.L2_orbit.period*(connection.copy_num_2));
t_vec = linspace(0,tof,5000);
C_R_vec = normrnd(C_R_avg, C_R_sigma, size(t_vec));

ode_opts = odeset('RelTol',1e-13,'AbsTol',1e-20);

X0 = [connection.connection_ms.X0; cubesat.mass/normalizers.m_norm; zeros(3,1)];
%%
tic
[~,X_SRP_stoch] = ode113(@(t,X) CR3BP_SRP_stoch(t,X,mu_SE,C_R_vec,t_vec,SRP_flux,A_sc,c), t_vec, X0, ode_opts);
%[~,X_noSRP] = ode113(@(t,X) CR3BP_SRP_stoch(t,X,mu_SE,C_R_vec,t_vec,0,A_sc,c), t_vec, X0);
[~,X_noSRP] = ode113(@(t,X) CR3BP(t,X,mu_SE), t_vec, connection.connection_ms.X0, ode_opts);
toc
%% Plot

figure
addToolbarExplorationButtons(gcf) % Adds buttons to figure toolbar
plot(1-mu_SE, 0, 'ok', 'markerfacecolor', 'b', 'markersize', 10, 'DisplayName', 'Earth'); hold on % Smaller primary
plot(x_L1, 0, 'dk', 'markerfacecolor', 'r', 'DisplayName', '$$L_1$$'); hold on % L1 location
plot(x_L2, 0, 'dk' , 'markerfacecolor', 'b', 'DisplayName', '$$L_2$$'); hold on % L2 location
scatter(connection.connection_ms.X0(1), connection.connection_ms.X0(2), 'x', 'DisplayName', 'Initial Position');
%scatter(target_state_full(1), target_state_full(2), target_state_full(3), 'x', 'DisplayName', 'Target Position');
%plot(X_SRP_stoch(:,1), X_SRP_stoch(:,2), 'm.-','DisplayName', 'SRP Trajectory');
plot(X_noSRP(:,1), X_noSRP(:,2), 'b.-','DisplayName', 'No SRP Trajectory');
hold off
title('SRP Lyapunov Connection')
xlabel('x')
ylabel('y')
zlabel('z')
grid on;
legend();
axis equal

%% MLE for C_R_avg

C_R_vary = linspace(0,2,500);
ll_plot = MLE_SRP(C_R_vary,X_SRP_stoch,C_R_sigma,mu_SE,C_R_vec,t_vec,SRP_flux,A_sc,c);

% MLE estimate for C_R
C_R_MLE = C_R_vary((ll_plot==max(ll_plot)));

% Plot log-likelihood over CR

max_C_R_label = "MLE $$C_R$$ = " + num2str(C_R_MLE);

figure
addToolbarExplorationButtons(gcf)
hold on
scatter(C_R_MLE, ll_plot((ll_plot==max(ll_plot))), 'mo', 'DisplayName',max_C_R_label)
plot(C_R_vary, ll_plot', 'r.-','DisplayName', 'Log-likelihood function')
hold off
xlabel('$$C_R$$')
grid on
ylabel('Log-likelihood')
legend()

