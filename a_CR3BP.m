function a_CR3BP = a_CR3BP(t,X,mu)

    % Returns CR3BP acceleration
    
    X = reshape(X,length(X),[]);

    % Positions
    x = X(1);
    y = X(2);
    z = X(3);

    % Velocities
    xdot = X(4);
    ydot = X(5);
    zdot = X(6);
    
    % Distances to primaries
    r1 = sqrt((x+mu)^2 + y^2 + z^2);
    R_sc = [x; y; z];
    R_sun = [-mu; 0; 0]; % Assuming Sun is first primary, DOES NOT WORK OTHERWISE
    R2 = R_sc - R_sun;
    r2 = norm(R2);

    % Accelerations 
    xddot = 2*ydot + x -(1-mu)*((x+mu)/(r1^3)) - mu*(x-1+mu)/(r2^3);
    yddot = -2*xdot + y - (1-mu)*(y/(r1^3)) - mu*(y)/(r2^3);
    zddot = -(1-mu)*(z)/(r1^3) - mu*(z)/(r2^3);
    a_CR3BP = [xddot; yddot; zddot];
    
end