% Level 2: model-based RL

%% dioxy
clear; close all; clc;
set(groot,'defaultLineLineWidth', 1.5)
set(groot,'defaultAxesFontSize', 16)
set(groot,'defaulttextinterpreter','latex');  
set(groot, 'defaultAxesTickLabelInterpreter','latex');  
set(groot, 'defaultLegendInterpreter','latex');
p = gcp;
addpath(genpath(pwd));

%% Load relevant data

load('environment_SE_spacecraft_smallsat.mat');
load('connection.mat');

%% SRP setup
c_light = 299792/normalizers.VU; % CR3BP normalized speed of light
SRP_flux_dim = 1361; % [W/m^2] = [kgm^2/s^3/m^2] = [kg/s^3]
flux_normalizer = normalizers.MU/normalizers.TU^3;
SRP_flux = SRP_flux_dim/flux_normalizer;
A_sc = 0.1/1000*0.2/1000/LU^2;
%A_sc = 1e-24;
C_R = 1.5;

%% Simulate multiple-shooting results to pick suitable start and end states
ode_opts = odeset('RelTol',1e-13,'AbsTol',1e-20);

[t_start,X_start] = ode113(@(t,X) CR3BP(t,X,mu_SE), [0 connection.connection_ms.total_time - (connection.L2_orbit.period*(connection.copy_num_2))], connection.connection_ms.X0, ode_opts);
[t_L2,X_L2] = ode113(@(t,X) CR3BP(t,X,mu_SE), [0 connection.L2_orbit.period], connection.L2_orbit.X0, ode_opts);

% Plot
figure
addToolbarExplorationButtons(gcf) % Adds buttons to figure toolbar
plot(1-mu_SE, 0, 'ok', 'markerfacecolor', 'b', 'markersize', 10, 'DisplayName', 'Earth'); hold on % Smaller primary
plot(x_L1, 0, 'dk', 'markerfacecolor', 'r', 'DisplayName', '$$L_1$$'); hold on % L1 location
plot(x_L2, 0, 'dk' , 'markerfacecolor', 'b', 'DisplayName', '$$L_2$$'); hold on % L2 location
scatter(connection.connection_ms.X0(1), connection.connection_ms.X0(2), 'x', 'DisplayName', 'Initial Position');
plot(X_start(:,1), X_start(:,2), 'b.-','DisplayName', 'Multiple-Shooting Trajectory');
plot(X_L2(:,1), X_L2(:,2), 'r.-','DisplayName', '$$L_2$$ Target Orbit');
hold off
title('Multiple-Shooting Lyapunov Connection')
xlabel('x')
ylabel('y')
zlabel('z')
grid on;
legend();
axis equal

start_index = 974;
end_index_L2 = 581;
end_index_ms = 1440;
tof = t_start(end_index_ms) - t_start(start_index);
initial_state = [X_start(974,:)'; m_sc];
target_state_posvel = X_L2(end_index_L2,:)';

% Simulate for chosen tof with start and endpoints indicated
[t_ig, X_ig] = ode113(@(t,X) CR3BP(t,X,mu_SE), [0 tof], initial_state(1:6), ode_opts);
[t_ig_SRP, X_ig_SRP] = ode113(@(t,X) CR3BP_SRP_cart_control_mex(t,X,mu_SE,exh_vel,max_thrust_mag,C_R,SRP_flux,A_sc,c_light), [0 tof], [initial_state; zeros(3,1)], ode_opts);

figure
addToolbarExplorationButtons(gcf) % Adds buttons to figure toolbar
plot(1-mu_SE, 0, 'ok', 'markerfacecolor', 'b', 'markersize', 10, 'DisplayName', 'Earth'); hold on % Smaller primary
plot(x_L1, 0, 'dk', 'markerfacecolor', 'r', 'DisplayName', '$$L_1$$'); % L1 location
plot(x_L2, 0, 'dk' , 'markerfacecolor', 'b', 'DisplayName', '$$L_2$$'); % L2 location
scatter(initial_state(1), initial_state(2), 'x', 'DisplayName', 'Initial Position');
scatter(target_state_posvel(1), target_state_posvel(2), 'x', 'DisplayName', 'Target state');
plot(X_ig(:,1), X_ig(:,2), 'b.-','DisplayName', 'Multiple-Shooting Trajectory');
plot(X_ig_SRP(:,1), X_ig_SRP(:,2), 'm.-','DisplayName', 'SRP Trajectory');
hold off
title('Multiple-Shooting Lyapunov Connection')
xlabel('x')
ylabel('y')
grid on;
legend();
axis equal

%% Set up DDP

% Create empty traj struct
num_stages = 200;
num_phases = 1;
traj = generate_traj(num_stages,num_phases);

% Assign values
traj.mu = mu_SE;
traj.exh_vel = exh_vel;
traj.max_thrust_mag = max_thrust_mag;
traj.C_R = C_R;
traj.Phi = SRP_flux;
traj.A_sc = A_sc;
traj.c_light = c_light;
traj.normalizers = normalizers;
traj.initial_state = initial_state;
traj.target_state = target_state_posvel;

% Acceptance bounds
traj.iterate_epsilon = 0.1; % acceptance bound for each iteration, rho must be within this of 1
traj.opt_epsilon = 1e-7; % stop condition for expected reduction
traj.feas_epsilon = 1e-7; % stop condition for endpoint constraint

% Penalty weight
traj.penalty_sigma = 1e1; % scaling parameter for quadratic penalty term

% TRQP parameters
traj.k_sigma = 1.1;
traj.kappa = 0.1; 
delta_TRQP_default = 1e-4;
traj.delta_TRQP_min = 5e-18;
traj.delta_TRQP_max = 1;
traj.delta_TRQP = delta_TRQP_default;

% Gain guard parameters
traj.eta1 = 10;
traj.eta2 = 1000;

% Multipliers
initial_lambda = zeros(6,1);
traj.lambda = initial_lambda;
traj.nominal_lambda = initial_lambda;

% Constraint vector function
traj.compute_constraintvec = @(traj) constraint_vec(traj,target_state_posvel);

% Times
stage_times = linspace(0,tof,num_stages);
traj.stage_times = stage_times;

% Loop through to initialize stages
traj.stage{1}.nominal_state = initial_state;
traj.stage{1}.state = initial_state;
traj.stage{1}.time = traj.stage_times(1);
traj.stage{1}.nominal_u = zeros(3,1);
traj.stage{1}.u = zeros(3,1);
current_state = initial_state;
for k = 1:num_stages-1
    [~,stage_states] = ode113(@(t,X) CR3BP_SRP_cart_control_mex(t,X,mu_SE,exh_vel,max_thrust_mag,C_R,SRP_flux,A_sc,c_light), [stage_times(k), stage_times(k+1)], [current_state; zeros(3,1)], ode_opts);
    current_state = stage_states(end,1:7)';
    traj.stage{k+1}.nominal_state = current_state;
    traj.stage{k+1}.state = current_state;
    traj.stage{k+1}.time = stage_times(k+1);
    traj.stage{k+1}.nominal_u = zeros(3,1);
    traj.stage{k+1}.u = zeros(3,1);
end

traj = traj.initialize(traj);
traj.J_nom = traj.compute_J(traj);
traj.f_nom = traj.compute_f(traj);
traj.h_nom = traj.compute_h(traj);

fprintf("Done initializing.\n");

%% Plot initial guess

ddp_traj_plot(traj);

%% DDP Loop

ddp_final_traj = ddp_func(traj,'max_iters',3000,'bool_liveplot',true);

%%
ddp_traj_plot(ddp_final_traj.traj);

%% Try stuff with noisy control

mod_traj = ddp_final_traj.traj;

mod_traj = forward_pass_control_stoch(mod_traj,0.1);
mod_traj.J_nom = traj.compute_J(mod_traj);
mod_traj.f_nom = traj.compute_f(mod_traj);
mod_traj.h_nom = traj.compute_h(mod_traj);

fprintf("Old feasibility: %d.\n New feasibility: %d.\n\n",ddp_final_traj.traj.f_nom,mod_traj.f_nom);
fprintf("Old cost: %f.\n New cost: %f.\n\n",ddp_final_traj.traj.J_nom,mod_traj.J_nom);

ddp_traj_plot(mod_traj);
%% Try stuff with noisy SRP (FAIL)

mod_traj = ddp_final_traj.traj;
C_R_avg = 1.5;
C_R_sigma = 0.1;

t_vec = linspace(0,tof,200);
C_R_vec = normrnd(C_R_avg, C_R_sigma, size(t_vec));
%C_R_vec = zeros(1,200);

mod_traj = forward_pass_SRP_stoch(mod_traj,C_R_vec,t_vec);
mod_traj.J_nom = traj.compute_J(mod_traj);
mod_traj.f_nom = traj.compute_f(mod_traj);
mod_traj.h_nom = traj.compute_h(mod_traj);
fprintf("Old feasibility: %d.\n New feasibility: %d.\n\n",ddp_final_traj.traj.f_nom,mod_traj.f_nom);
fprintf("Old cost: %f.\n New cost: %f.\n\n",ddp_final_traj.traj.J_nom,mod_traj.J_nom);
ddp_traj_plot(mod_traj);

%%
for k = 1:mod_traj.num_stages
    mod_traj.stage{k}.nominal_state = mod_traj.stage{k}.state;
    mod_traj.stage{k}.nominal_u = mod_traj.stage{k}.u;
    mod_traj.stage{k}.deltax_prev = mod_traj.stage{k}.deltax;
end
mod_traj.penalty_sigma = 1e-1;
mod_traj.delta_TRQP = delta_TRQP_default;
mod_traj = traj.initialize(mod_traj);
mod_traj.J_nom = traj.compute_J(mod_traj);
mod_traj.f_nom = traj.compute_f(mod_traj);
mod_traj.h_nom = traj.compute_h(mod_traj);

ddp_mod = ddp_func(mod_traj,'max_iters',3000,'bool_liveplot',true);
ddp_traj_plot(ddp_mod.traj);

%%

C_R_vary = linspace(1,2,100);
C_R_sigma = 0.1;
ll_plot = MLE_SRP_DDP(mod_traj,50,C_R_vary,C_R_sigma,mu_SE,C_R_vec,t_vec,SRP_flux,A_sc,c_light);

% MLE estimate for C_R
C_R_MLE = C_R_vary((ll_plot==max(ll_plot)));

% Plot log-likelihood over CR

max_C_R_label = "MLE $$C_R$$ = " + num2str(C_R_MLE);

figure
addToolbarExplorationButtons(gcf)
hold on
scatter(C_R_MLE, ll_plot((ll_plot==max(ll_plot))), 'mo', 'DisplayName',max_C_R_label)
plot(C_R_vary, ll_plot', 'r.-','DisplayName', 'Log-likelihood function')
hold off
xlabel('$$C_R$$')
grid on
ylabel('Log-likelihood')
legend()

%%
for k = 1:mod_traj.num_stages
    mod_traj.stage{k}.nominal_state = mod_traj.stage{k}.state;
    mod_traj.stage{k}.nominal_u = mod_traj.stage{k}.u;
    mod_traj.stage{k}.deltax_prev = mod_traj.stage{k}.deltax;
end
mod_traj.penalty_sigma = 1e-1;
mod_traj.delta_TRQP = delta_TRQP_default;
mod_traj = traj.initialize(mod_traj);
mod_traj.J_nom = traj.compute_J(mod_traj);
mod_traj.f_nom = traj.compute_f(mod_traj);
mod_traj.h_nom = traj.compute_h(mod_traj);

ddp_mod = ddp_func(mod_traj,'max_iters',3000,'bool_liveplot',true);
ddp_traj_plot(ddp_mod.traj);

%% Receding Horizon

C_R_avg = 1.5;
C_R_sigma = 0.1;

t_vec = linspace(0,tof,200);
C_R_vec = normrnd(C_R_avg, C_R_sigma, size(t_vec));
RL_C_R_vec = C_R_vec;
RL_t_vec = t_vec;
base_traj = forward_pass_SRP_stoch(ddp_final_traj.traj,RL_C_R_vec,t_vec); % FIX THIS (do i do noisy forward pass first?)
base_traj.J_nom = base_traj.compute_J(base_traj);
base_traj.f_nom = base_traj.compute_f(base_traj);
base_traj.h_nom = base_traj.compute_h(base_traj);
for k = 1:base_traj.num_stages
    base_traj.stage{k}.nominal_state = base_traj.stage{k}.state;
    base_traj.stage{k}.nominal_u = base_traj.stage{k}.u;
    base_traj.stage{k}.deltax_prev = base_traj.stage{k}.deltax;
end
RL_traj = base_traj;

seg_size = 40;
num_segs = base_traj.num_stages/seg_size + 1;

states = [];
controls = [];
for k = 1:seg_size-1
    states = [states, RL_traj.stage{k}.nominal_state];
    controls = [controls, RL_traj.stage{k}.nominal_u];
end
C_R_hist = [C_R_avg];
%%
num_stages_togo = RL_traj.num_stages;
last_seg = false;
RL_DDP = tic;
while num_stages_togo > 0
    
    %% MLE
    C_R_vary = linspace(1,2,50000); 
    ll_plot = MLE_SRP_DDP(RL_traj,seg_size,C_R_vary,C_R_sigma,mu_SE,RL_C_R_vec,RL_t_vec,SRP_flux,A_sc,c_light);
    C_R_MLE = C_R_vary((ll_plot==max(ll_plot)));
    RL_traj.C_R = C_R_MLE;
    C_R_hist = [C_R_hist; C_R_MLE];
    %% Receding horizon
    % Shrink trajectory
    RL_traj_old = RL_traj;
    RL_traj.initial_state = RL_traj_old.stage{seg_size}.state;
    if num_stages_togo > 2*seg_size
        num_stages_togo = RL_traj_old.num_stages - seg_size + 1;
    else
        num_stages_togo = RL_traj_old.num_stages;
        last_seg = true;
    end
    for k = 1:num_stages_togo
        RL_traj.stage{k} = RL_traj_old.stage{seg_size+k-1};
    end
    RL_traj.stage(num_stages_togo+1:end) = [];
    RL_traj.num_stages = num_stages_togo;
    RL_traj.stage_times = RL_traj_old.stage_times(seg_size:end);
    RL_C_R_vec = RL_C_R_vec(seg_size:end);
    RL_t_vec = RL_t_vec(seg_size:end);
    
    % Do DDP
    initial_lambda = zeros(6,1);
    RL_traj.lambda = initial_lambda;
    RL_traj.nominal_lambda = initial_lambda;
    RL_traj.penalty_sigma = 1e1;
    RL_traj.delta_TRQP = 1e-4;%delta_TRQP_default;
    RL_traj.stage{1}.nominal_state = RL_traj.initial_state;
    RL_traj.stage{1}.state = RL_traj.initial_state;
    RL_traj.stage{1}.time = RL_traj.stage_times(1);
    RL_traj.stage{1}.nominal_u = zeros(3,1);
    RL_traj.stage{1}.u = zeros(3,1);
    current_state = RL_traj.initial_state;
    for k = 1:RL_traj.num_stages-1
        [~,stage_states] = ode113(@(t,X) CR3BP_SRP_cart_control_mex(t,X,mu_SE,exh_vel,max_thrust_mag,RL_traj.C_R,RL_traj.Phi,RL_traj.A_sc,RL_traj.c_light), [stage_times(k), stage_times(k+1)], [current_state; zeros(3,1)], ode_opts);
        current_state = stage_states(end,1:7)';
        RL_traj.stage{k+1}.nominal_state = current_state;
        RL_traj.stage{k+1}.state = current_state;
        RL_traj.stage{k+1}.time = RL_traj.stage_times(k+1);
        RL_traj.stage{k+1}.nominal_u = zeros(3,1);
        RL_traj.stage{k+1}.u = zeros(3,1);
    end

    RL_traj = RL_traj.initialize(RL_traj);
    RL_traj.J_nom = RL_traj.compute_J(RL_traj);
    RL_traj.f_nom = RL_traj.compute_f(RL_traj);
    RL_traj.h_nom = RL_traj.compute_h(RL_traj);

    ddp_mod = ddp_func(RL_traj,'max_iters',3000,'bool_liveplot',true);
    
    RL_traj = forward_pass_SRP_stoch(ddp_mod.traj,RL_C_R_vec,RL_t_vec);
    
    if ~last_seg
        for k = 1:seg_size-1
            states = [states, RL_traj.stage{k}.nominal_state];
            controls = [controls, RL_traj.stage{k}.nominal_u];
        end
    else
        for k = 1:RL_traj.num_stages
            states = [states, RL_traj.stage{k}.nominal_state];
            controls = [controls, RL_traj.stage{k}.nominal_u];
        end
    end
    
    % Check for ending (compensating for bad loop logic to begin with)
    if RL_traj.num_stages <= 2*seg_size
        fprintf("Done.\n\n");
        break
    end
    
end
toc(RL_DDP);
%%
fig_traj = figure;
addToolbarExplorationButtons(fig_traj)
scatter(x_L1, 0, 'rd', 'DisplayName','L1'); hold on
scatter(x_L2, 0, 'bd', 'DisplayName','L2');
plot(1-traj.mu, 0, 'ok', 'markerfacecolor', 'b', 'markersize', 10, 'DisplayName', 'Earth'); hold on % Smaller primary
scatter(initial_state(1), initial_state(2), 'co','filled','DisplayName','Initial State');
scatter(target_state_posvel(1), target_state_posvel(2), 'mo','filled','DisplayName', 'Target State');
plot(states(1,:), states(2,:), '-o', 'DisplayName','Trajectory');
quiver(states(1,:), states(2,:), controls(1,:), controls(2,:), 1.1, 'DisplayName', 'Thrust Vectors');
hold off
grid on
legend();
axis equal
xlabel('x')
ylabel('y')

fig_control = figure;
addToolbarExplorationButtons(fig_control)
plot(traj.stage_times(1:traj.num_stages).*TU/60/60/24, traj.max_thrust_mag*FU*1000*1000.*controls(1,:), '-o','DisplayName','x thrust'); hold on
plot(traj.stage_times(1:traj.num_stages).*TU/60/60/24, traj.max_thrust_mag*FU*1000*1000.*controls(2,:), '-o','DisplayName','y thrust');
plot(traj.stage_times(1:traj.num_stages).*TU/60/60/24, traj.max_thrust_mag*FU*1000*1000.*controls(3,:), '-o','DisplayName','z thrust'); hold off
grid on
legend()
title('Control History')
ylabel('Thrust [mN]')
xlabel('Time [days]')

%% Comparison to first DDP opt
u_c = NaN(traj.nu,traj.num_stages);
for k = 1:traj.num_stages
    u_c(:,k) = traj.max_thrust_mag*FU*1000*1000.*(traj.stage{k}.nominal_u) - controls(:,k);%-traj.stage{k}.nominal_u); % FU is in kN; multiply by FU to get kN, then convert to mN
    %fprintf("u diff at stage %i is:\n %d\n%d\n%d\n",k,u(1,k),u(2,k),u(3,k));
end

states_c = NaN(traj.nx,traj.num_stages);
for k = 1:traj.num_stages
    states_c(:,k) = traj.stage{k}.state - states(:,k);
end

fig_traj_pos = figure;
addToolbarExplorationButtons(fig_traj_pos); hold on
plot(traj.stage_times(1:traj.num_stages).*TU/60/60/24, states_c(1,:), '-o','DisplayName','x difference');
plot(traj.stage_times(1:traj.num_stages).*TU/60/60/24, states_c(2,:), '-o','DisplayName','y difference');
plot(traj.stage_times(1:traj.num_stages).*TU/60/60/24, states_c(3,:), '-o','DisplayName','z difference');
grid on
legend()
xlabel('Time [days]')
ylabel('Position Differences')
title('Position Differences Before and After RL')

fig_traj_vel = figure;
addToolbarExplorationButtons(fig_traj_vel); hold on
plot(traj.stage_times(1:traj.num_stages).*TU/60/60/24, states_c(4,:), '-o','DisplayName','$$\dot{x}$$ difference');
plot(traj.stage_times(1:traj.num_stages).*TU/60/60/24, states_c(5,:), '-o','DisplayName','$$\dot{y}$$ difference');
plot(traj.stage_times(1:traj.num_stages).*TU/60/60/24, states_c(6,:), '-o','DisplayName','$$\dot{z}$$ difference');

grid on
legend()
xlabel('Time [days]')
ylabel('Position Differences')
title('Velocity Differences Before and After RL')


fig_control = figure;
addToolbarExplorationButtons(fig_control)
plot(traj.stage_times(1:traj.num_stages).*TU/60/60/24, u_c(1,:), '-o','DisplayName','x thrust'); hold on
plot(traj.stage_times(1:traj.num_stages).*TU/60/60/24, u_c(2,:), '-o','DisplayName','y thrust');
plot(traj.stage_times(1:traj.num_stages).*TU/60/60/24, u_c(3,:), '-o','DisplayName','z thrust'); hold off
grid on
legend()
title('Control History Difference')
ylabel('Thrust [mN]')
xlabel('Time [days]')
title('Control Differences Before and After RL')

%% CR Estimates
figure
addToolbarExplorationButtons(gcf)
plot(1:1:5,C_R_hist,'-o','DisplayName','$$C_R$$ Estimate')
xlabel('RL Iteration')
ylabel('$$C_R$$')
title('$$C_R$$ Estimate Over Each RL Iteration')
grid on
legend()
