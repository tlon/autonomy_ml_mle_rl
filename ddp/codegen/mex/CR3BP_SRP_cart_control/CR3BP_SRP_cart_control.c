/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * CR3BP_SRP_cart_control.c
 *
 * Code generation for function 'CR3BP_SRP_cart_control'
 *
 */

/* Include files */
#include "mwmathutil.h"
#include "rt_nonfinite.h"
#include "CR3BP_SRP_cart_control.h"
#include "sqrt.h"
#include "CR3BP_SRP_cart_control_data.h"

/* Variable Definitions */
static emlrtRSInfo emlrtRSI = { 25,    /* lineNo */
  "CR3BP_SRP_cart_control",            /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\CR3BP_SRP_cart_control.m"   /* pathName */
};

static emlrtRSInfo b_emlrtRSI = { 26,  /* lineNo */
  "CR3BP_SRP_cart_control",            /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\CR3BP_SRP_cart_control.m"   /* pathName */
};

static emlrtRSInfo c_emlrtRSI = { 29,  /* lineNo */
  "CR3BP_SRP_cart_control",            /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\CR3BP_SRP_cart_control.m"   /* pathName */
};

static emlrtRSInfo d_emlrtRSI = { 30,  /* lineNo */
  "CR3BP_SRP_cart_control",            /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\CR3BP_SRP_cart_control.m"   /* pathName */
};

static emlrtRSInfo e_emlrtRSI = { 31,  /* lineNo */
  "CR3BP_SRP_cart_control",            /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\CR3BP_SRP_cart_control.m"   /* pathName */
};

static emlrtRSInfo f_emlrtRSI = { 39,  /* lineNo */
  "CR3BP_SRP_cart_control",            /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\CR3BP_SRP_cart_control.m"   /* pathName */
};

static emlrtRSInfo g_emlrtRSI = { 42,  /* lineNo */
  "CR3BP_SRP_cart_control",            /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\CR3BP_SRP_cart_control.m"   /* pathName */
};

static emlrtRSInfo h_emlrtRSI = { 43,  /* lineNo */
  "CR3BP_SRP_cart_control",            /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\CR3BP_SRP_cart_control.m"   /* pathName */
};

static emlrtRSInfo i_emlrtRSI = { 44,  /* lineNo */
  "CR3BP_SRP_cart_control",            /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\CR3BP_SRP_cart_control.m"   /* pathName */
};

static emlrtRSInfo j_emlrtRSI = { 45,  /* lineNo */
  "mpower",                            /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\ops\\mpower.m"/* pathName */
};

static emlrtRSInfo k_emlrtRSI = { 55,  /* lineNo */
  "power",                             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\ops\\power.m"/* pathName */
};

static emlrtRTEInfo emlrtRTEI = { 67,  /* lineNo */
  5,                                   /* colNo */
  "fltpower",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\ops\\power.m"/* pName */
};

/* Function Definitions */
void CR3BP_SRP_cart_control(const emlrtStack *sp, real_T t, const real_T X[10],
  real_T mu, real_T c, real_T Tmax, real_T C_R, real_T Phi, real_T A_sc, real_T
  c_light, real_T X_dot[10])
{
  real_T a_tmp;
  real_T r1_tmp;
  real_T b_r1_tmp;
  real_T r1;
  real_T b_a_tmp;
  real_T r2;
  real_T x;
  real_T a;
  real_T b_a;
  real_T c_a;
  emlrtStack st;
  emlrtStack b_st;
  emlrtStack c_st;
  (void)t;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;

  /*  Reshape to column vector */
  /*  Positions */
  /*  Velocities */
  /*  Mass */
  /*  Control */
  /*  Distances to primaries */
  st.site = &emlrtRSI;
  a_tmp = X[0] + mu;
  b_st.site = &j_emlrtRSI;
  st.site = &emlrtRSI;
  b_st.site = &j_emlrtRSI;
  st.site = &emlrtRSI;
  b_st.site = &j_emlrtRSI;
  r1_tmp = X[1] * X[1];
  b_r1_tmp = X[2] * X[2];
  r1 = (a_tmp * a_tmp + r1_tmp) + b_r1_tmp;
  st.site = &emlrtRSI;
  b_sqrt(&st, &r1);
  st.site = &b_emlrtRSI;
  b_a_tmp = (X[0] - 1.0) + mu;
  b_st.site = &j_emlrtRSI;
  st.site = &b_emlrtRSI;
  b_st.site = &j_emlrtRSI;
  st.site = &b_emlrtRSI;
  b_st.site = &j_emlrtRSI;
  r2 = (b_a_tmp * b_a_tmp + r1_tmp) + b_r1_tmp;
  st.site = &b_emlrtRSI;
  b_sqrt(&st, &r2);

  /*  Accelerations  */
  st.site = &c_emlrtRSI;
  b_st.site = &j_emlrtRSI;
  st.site = &c_emlrtRSI;
  b_st.site = &j_emlrtRSI;
  st.site = &d_emlrtRSI;
  b_st.site = &j_emlrtRSI;
  st.site = &d_emlrtRSI;
  b_st.site = &j_emlrtRSI;
  st.site = &e_emlrtRSI;
  b_st.site = &j_emlrtRSI;
  st.site = &e_emlrtRSI;
  b_st.site = &j_emlrtRSI;

  /*  Control */
  /*  Mass Flow Rate */
  st.site = &f_emlrtRSI;
  b_st.site = &j_emlrtRSI;
  st.site = &f_emlrtRSI;
  b_st.site = &j_emlrtRSI;
  st.site = &f_emlrtRSI;
  b_st.site = &j_emlrtRSI;
  st.site = &f_emlrtRSI;
  x = ((X[7] * X[7] + X[8] * X[8]) + X[9] * X[9]) + 1.0E-8;
  if (x < 0.0) {
    emlrtErrorWithMessageIdR2018a(&st, &b_emlrtRTEI,
      "Coder:toolbox:ElFunDomainError", "Coder:toolbox:ElFunDomainError", 3, 4,
      4, "sqrt");
  }

  x = muDoubleScalarSqrt(x);

  /*  SRP */
  st.site = &g_emlrtRSI;
  b_st.site = &j_emlrtRSI;
  st.site = &g_emlrtRSI;
  b_st.site = &j_emlrtRSI;
  st.site = &g_emlrtRSI;
  b_st.site = &j_emlrtRSI;
  st.site = &g_emlrtRSI;
  a = (a_tmp * a_tmp + r1_tmp) + b_r1_tmp;
  b_st.site = &j_emlrtRSI;
  c_st.site = &k_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &h_emlrtRSI;
  b_st.site = &j_emlrtRSI;
  st.site = &h_emlrtRSI;
  b_st.site = &j_emlrtRSI;
  st.site = &h_emlrtRSI;
  b_st.site = &j_emlrtRSI;
  st.site = &h_emlrtRSI;
  b_a = (a_tmp * a_tmp + r1_tmp) + b_r1_tmp;
  b_st.site = &j_emlrtRSI;
  c_st.site = &k_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &i_emlrtRSI;
  b_st.site = &j_emlrtRSI;
  st.site = &i_emlrtRSI;
  b_st.site = &j_emlrtRSI;
  st.site = &i_emlrtRSI;
  b_st.site = &j_emlrtRSI;
  st.site = &i_emlrtRSI;
  c_a = (a_tmp * a_tmp + r1_tmp) + b_r1_tmp;
  b_st.site = &j_emlrtRSI;
  c_st.site = &k_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  /*  Total Acceleration */
  r1 = muDoubleScalarPower(r1, 3.0);
  r1_tmp = muDoubleScalarPower(r2, 3.0);
  b_r1_tmp = C_R * Phi;
  X_dot[3] = ((((2.0 * X[4] + X[0]) - (1.0 - mu) * (a_tmp / r1)) - mu * b_a_tmp /
               r1_tmp) + b_r1_tmp / (c_light * muDoubleScalarPower(a, 1.5)) *
              A_sc / X[6] * a_tmp) + Tmax * (X[7] / X[6]);
  X_dot[4] = ((((-2.0 * X[3] + X[1]) - (1.0 - mu) * (X[1] / r1)) - mu * X[1] /
               r1_tmp) + b_r1_tmp / (c_light * muDoubleScalarPower(b_a, 1.5)) *
              A_sc / X[6] * X[1]) + Tmax * (X[8] / X[6]);
  X_dot[5] = ((-(1.0 - mu) * X[2] / r1 - mu * X[2] / r1_tmp) + b_r1_tmp /
              (c_light * muDoubleScalarPower(c_a, 1.5)) * A_sc / X[6] * X[2]) +
    Tmax * (X[9] / X[6]);
  X_dot[6] = -Tmax * x / c;
  X_dot[0] = X[3];
  X_dot[7] = 0.0;
  X_dot[1] = X[4];
  X_dot[8] = 0.0;
  X_dot[2] = X[5];
  X_dot[9] = 0.0;
}

/* End of code generation (CR3BP_SRP_cart_control.c) */
