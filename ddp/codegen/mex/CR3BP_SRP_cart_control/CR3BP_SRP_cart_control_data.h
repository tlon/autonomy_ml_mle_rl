/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * CR3BP_SRP_cart_control_data.h
 *
 * Code generation for function 'CR3BP_SRP_cart_control_data'
 *
 */

#ifndef CR3BP_SRP_CART_CONTROL_DATA_H
#define CR3BP_SRP_CART_CONTROL_DATA_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "CR3BP_SRP_cart_control_types.h"

/* Variable Declarations */
extern emlrtCTX emlrtRootTLSGlobal;
extern emlrtContext emlrtContextGlobal;
extern emlrtRTEInfo b_emlrtRTEI;

#endif

/* End of code generation (CR3BP_SRP_cart_control_data.h) */
