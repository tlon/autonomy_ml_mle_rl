/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * fXX_SRP.c
 *
 * Code generation for function 'fXX_SRP'
 *
 */

/* Include files */
#include "mwmathutil.h"
#include "rt_nonfinite.h"
#include "CR3BP_SRP_cart_control_STMSTT.h"
#include "fXX_SRP.h"
#include "CR3BP_SRP_cart_control_STMSTT_data.h"

/* Variable Definitions */
static emlrtRSInfo mb_emlrtRSI = { 350,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo nb_emlrtRSI = { 351,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo ob_emlrtRSI = { 352,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo pb_emlrtRSI = { 356,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo qb_emlrtRSI = { 361,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo rb_emlrtRSI = { 362,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo sb_emlrtRSI = { 363,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo tb_emlrtRSI = { 367,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo ub_emlrtRSI = { 372,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo vb_emlrtRSI = { 373,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo wb_emlrtRSI = { 374,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo xb_emlrtRSI = { 378,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo yb_emlrtRSI = { 416,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo ac_emlrtRSI = { 417,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo bc_emlrtRSI = { 418,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo cc_emlrtRSI = { 422,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo dc_emlrtRSI = { 423,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo ec_emlrtRSI = { 433,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo fc_emlrtRSI = { 462,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo gc_emlrtRSI = { 463,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo hc_emlrtRSI = { 464,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo ic_emlrtRSI = { 468,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo jc_emlrtRSI = { 473,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo kc_emlrtRSI = { 474,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo lc_emlrtRSI = { 475,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo mc_emlrtRSI = { 479,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo nc_emlrtRSI = { 484,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo oc_emlrtRSI = { 485,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo pc_emlrtRSI = { 486,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo qc_emlrtRSI = { 490,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo rc_emlrtRSI = { 528,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo sc_emlrtRSI = { 529,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo tc_emlrtRSI = { 530,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo uc_emlrtRSI = { 534,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo vc_emlrtRSI = { 536,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo wc_emlrtRSI = { 556,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo xc_emlrtRSI = { 574,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo yc_emlrtRSI = { 575,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo ad_emlrtRSI = { 576,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo bd_emlrtRSI = { 580,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo cd_emlrtRSI = { 585,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo dd_emlrtRSI = { 586,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo ed_emlrtRSI = { 587,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo fd_emlrtRSI = { 591,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo gd_emlrtRSI = { 596,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo hd_emlrtRSI = { 597,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo id_emlrtRSI = { 598,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo jd_emlrtRSI = { 602,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo kd_emlrtRSI = { 640,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo ld_emlrtRSI = { 641,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo md_emlrtRSI = { 642,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo nd_emlrtRSI = { 646,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo od_emlrtRSI = { 649,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo pd_emlrtRSI = { 679,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo qd_emlrtRSI = { 770,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo rd_emlrtRSI = { 771,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo sd_emlrtRSI = { 772,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo td_emlrtRSI = { 781,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo ud_emlrtRSI = { 782,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo vd_emlrtRSI = { 783,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo wd_emlrtRSI = { 792,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo xd_emlrtRSI = { 793,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

static emlrtRSInfo yd_emlrtRSI = { 794,/* lineNo */
  "fXX_SRP",                           /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fXX_SRP.m"                  /* pathName */
};

/* Function Definitions */
void fXX_SRP(const emlrtStack *sp, const real_T X[1110], real_T c, real_T mu,
             real_T Tmax, real_T C_R, real_T Phi, real_T A_sc, real_T c_light,
             real_T fXX[1000])
{
  real_T a_tmp;
  real_T b_a_tmp;
  real_T c_a_tmp;
  real_T a;
  real_T b_a;
  real_T c_a;
  real_T d_a;
  real_T d_a_tmp;
  real_T e_a;
  real_T f_a;
  real_T g_a;
  real_T h_a;
  real_T i_a;
  real_T fXX_tmp;
  real_T b_fXX_tmp;
  real_T c_fXX_tmp;
  real_T d_fXX_tmp;
  real_T e_fXX_tmp;
  real_T f_fXX_tmp;
  real_T g_fXX_tmp;
  real_T h_fXX_tmp;
  real_T i_fXX_tmp;
  real_T j_fXX_tmp;
  real_T k_fXX_tmp;
  real_T l_fXX_tmp;
  real_T m_fXX_tmp;
  real_T n_fXX_tmp;
  real_T fXX_tmp_tmp;
  real_T o_fXX_tmp;
  real_T p_fXX_tmp;
  real_T b_fXX_tmp_tmp;
  real_T q_fXX_tmp;
  real_T r_fXX_tmp;
  real_T s_fXX_tmp;
  real_T t_fXX_tmp;
  real_T c_fXX_tmp_tmp;
  real_T u_fXX_tmp;
  real_T v_fXX_tmp;
  real_T d_fXX_tmp_tmp;
  real_T w_fXX_tmp;
  real_T x_fXX_tmp;
  real_T y_fXX_tmp;
  real_T ab_fXX_tmp;
  real_T bb_fXX_tmp;
  real_T e_fXX_tmp_tmp;
  real_T cb_fXX_tmp;
  real_T db_fXX_tmp;
  real_T eb_fXX_tmp;
  real_T fb_fXX_tmp;
  real_T gb_fXX_tmp;
  emlrtStack st;
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  emlrtStack e_st;
  emlrtStack f_st;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  e_st.prev = &d_st;
  e_st.tls = d_st.tls;
  f_st.prev = &e_st;
  f_st.tls = e_st.tls;
  fXX[0] = 0.0;
  fXX[100] = 0.0;
  fXX[200] = 0.0;
  fXX[300] = 0.0;
  fXX[400] = 0.0;
  fXX[500] = 0.0;
  fXX[600] = 0.0;
  fXX[700] = 0.0;
  fXX[800] = 0.0;
  fXX[900] = 0.0;
  fXX[10] = 0.0;
  fXX[110] = 0.0;
  fXX[210] = 0.0;
  fXX[310] = 0.0;
  fXX[410] = 0.0;
  fXX[510] = 0.0;
  fXX[610] = 0.0;
  fXX[710] = 0.0;
  fXX[810] = 0.0;
  fXX[910] = 0.0;
  fXX[20] = 0.0;
  fXX[120] = 0.0;
  fXX[220] = 0.0;
  fXX[320] = 0.0;
  fXX[420] = 0.0;
  fXX[520] = 0.0;
  fXX[620] = 0.0;
  fXX[720] = 0.0;
  fXX[820] = 0.0;
  fXX[920] = 0.0;
  fXX[30] = 0.0;
  fXX[130] = 0.0;
  fXX[230] = 0.0;
  fXX[330] = 0.0;
  fXX[430] = 0.0;
  fXX[530] = 0.0;
  fXX[630] = 0.0;
  fXX[730] = 0.0;
  fXX[830] = 0.0;
  fXX[930] = 0.0;
  fXX[40] = 0.0;
  fXX[140] = 0.0;
  fXX[240] = 0.0;
  fXX[340] = 0.0;
  fXX[440] = 0.0;
  fXX[540] = 0.0;
  fXX[640] = 0.0;
  fXX[740] = 0.0;
  fXX[840] = 0.0;
  fXX[940] = 0.0;
  fXX[50] = 0.0;
  fXX[150] = 0.0;
  fXX[250] = 0.0;
  fXX[350] = 0.0;
  fXX[450] = 0.0;
  fXX[550] = 0.0;
  fXX[650] = 0.0;
  fXX[750] = 0.0;
  fXX[850] = 0.0;
  fXX[950] = 0.0;
  fXX[60] = 0.0;
  fXX[160] = 0.0;
  fXX[260] = 0.0;
  fXX[360] = 0.0;
  fXX[460] = 0.0;
  fXX[560] = 0.0;
  fXX[660] = 0.0;
  fXX[760] = 0.0;
  fXX[860] = 0.0;
  fXX[960] = 0.0;
  fXX[70] = 0.0;
  fXX[170] = 0.0;
  fXX[270] = 0.0;
  fXX[370] = 0.0;
  fXX[470] = 0.0;
  fXX[570] = 0.0;
  fXX[670] = 0.0;
  fXX[770] = 0.0;
  fXX[870] = 0.0;
  fXX[970] = 0.0;
  fXX[80] = 0.0;
  fXX[180] = 0.0;
  fXX[280] = 0.0;
  fXX[380] = 0.0;
  fXX[480] = 0.0;
  fXX[580] = 0.0;
  fXX[680] = 0.0;
  fXX[780] = 0.0;
  fXX[880] = 0.0;
  fXX[980] = 0.0;
  fXX[90] = 0.0;
  fXX[190] = 0.0;
  fXX[290] = 0.0;
  fXX[390] = 0.0;
  fXX[490] = 0.0;
  fXX[590] = 0.0;
  fXX[690] = 0.0;
  fXX[790] = 0.0;
  fXX[890] = 0.0;
  fXX[990] = 0.0;
  fXX[1] = 0.0;
  fXX[101] = 0.0;
  fXX[201] = 0.0;
  fXX[301] = 0.0;
  fXX[401] = 0.0;
  fXX[501] = 0.0;
  fXX[601] = 0.0;
  fXX[701] = 0.0;
  fXX[801] = 0.0;
  fXX[901] = 0.0;
  fXX[11] = 0.0;
  fXX[111] = 0.0;
  fXX[211] = 0.0;
  fXX[311] = 0.0;
  fXX[411] = 0.0;
  fXX[511] = 0.0;
  fXX[611] = 0.0;
  fXX[711] = 0.0;
  fXX[811] = 0.0;
  fXX[911] = 0.0;
  fXX[21] = 0.0;
  fXX[121] = 0.0;
  fXX[221] = 0.0;
  fXX[321] = 0.0;
  fXX[421] = 0.0;
  fXX[521] = 0.0;
  fXX[621] = 0.0;
  fXX[721] = 0.0;
  fXX[821] = 0.0;
  fXX[921] = 0.0;
  fXX[31] = 0.0;
  fXX[131] = 0.0;
  fXX[231] = 0.0;
  fXX[331] = 0.0;
  fXX[431] = 0.0;
  fXX[531] = 0.0;
  fXX[631] = 0.0;
  fXX[731] = 0.0;
  fXX[831] = 0.0;
  fXX[931] = 0.0;
  fXX[41] = 0.0;
  fXX[141] = 0.0;
  fXX[241] = 0.0;
  fXX[341] = 0.0;
  fXX[441] = 0.0;
  fXX[541] = 0.0;
  fXX[641] = 0.0;
  fXX[741] = 0.0;
  fXX[841] = 0.0;
  fXX[941] = 0.0;
  fXX[51] = 0.0;
  fXX[151] = 0.0;
  fXX[251] = 0.0;
  fXX[351] = 0.0;
  fXX[451] = 0.0;
  fXX[551] = 0.0;
  fXX[651] = 0.0;
  fXX[751] = 0.0;
  fXX[851] = 0.0;
  fXX[951] = 0.0;
  fXX[61] = 0.0;
  fXX[161] = 0.0;
  fXX[261] = 0.0;
  fXX[361] = 0.0;
  fXX[461] = 0.0;
  fXX[561] = 0.0;
  fXX[661] = 0.0;
  fXX[761] = 0.0;
  fXX[861] = 0.0;
  fXX[961] = 0.0;
  fXX[71] = 0.0;
  fXX[171] = 0.0;
  fXX[271] = 0.0;
  fXX[371] = 0.0;
  fXX[471] = 0.0;
  fXX[571] = 0.0;
  fXX[671] = 0.0;
  fXX[771] = 0.0;
  fXX[871] = 0.0;
  fXX[971] = 0.0;
  fXX[81] = 0.0;
  fXX[181] = 0.0;
  fXX[281] = 0.0;
  fXX[381] = 0.0;
  fXX[481] = 0.0;
  fXX[581] = 0.0;
  fXX[681] = 0.0;
  fXX[781] = 0.0;
  fXX[881] = 0.0;
  fXX[981] = 0.0;
  fXX[91] = 0.0;
  fXX[191] = 0.0;
  fXX[291] = 0.0;
  fXX[391] = 0.0;
  fXX[491] = 0.0;
  fXX[591] = 0.0;
  fXX[691] = 0.0;
  fXX[791] = 0.0;
  fXX[891] = 0.0;
  fXX[991] = 0.0;
  fXX[2] = 0.0;
  fXX[102] = 0.0;
  fXX[202] = 0.0;
  fXX[302] = 0.0;
  fXX[402] = 0.0;
  fXX[502] = 0.0;
  fXX[602] = 0.0;
  fXX[702] = 0.0;
  fXX[802] = 0.0;
  fXX[902] = 0.0;
  fXX[12] = 0.0;
  fXX[112] = 0.0;
  fXX[212] = 0.0;
  fXX[312] = 0.0;
  fXX[412] = 0.0;
  fXX[512] = 0.0;
  fXX[612] = 0.0;
  fXX[712] = 0.0;
  fXX[812] = 0.0;
  fXX[912] = 0.0;
  fXX[22] = 0.0;
  fXX[122] = 0.0;
  fXX[222] = 0.0;
  fXX[322] = 0.0;
  fXX[422] = 0.0;
  fXX[522] = 0.0;
  fXX[622] = 0.0;
  fXX[722] = 0.0;
  fXX[822] = 0.0;
  fXX[922] = 0.0;
  fXX[32] = 0.0;
  fXX[132] = 0.0;
  fXX[232] = 0.0;
  fXX[332] = 0.0;
  fXX[432] = 0.0;
  fXX[532] = 0.0;
  fXX[632] = 0.0;
  fXX[732] = 0.0;
  fXX[832] = 0.0;
  fXX[932] = 0.0;
  fXX[42] = 0.0;
  fXX[142] = 0.0;
  fXX[242] = 0.0;
  fXX[342] = 0.0;
  fXX[442] = 0.0;
  fXX[542] = 0.0;
  fXX[642] = 0.0;
  fXX[742] = 0.0;
  fXX[842] = 0.0;
  fXX[942] = 0.0;
  fXX[52] = 0.0;
  fXX[152] = 0.0;
  fXX[252] = 0.0;
  fXX[352] = 0.0;
  fXX[452] = 0.0;
  fXX[552] = 0.0;
  fXX[652] = 0.0;
  fXX[752] = 0.0;
  fXX[852] = 0.0;
  fXX[952] = 0.0;
  fXX[62] = 0.0;
  fXX[162] = 0.0;
  fXX[262] = 0.0;
  fXX[362] = 0.0;
  fXX[462] = 0.0;
  fXX[562] = 0.0;
  fXX[662] = 0.0;
  fXX[762] = 0.0;
  fXX[862] = 0.0;
  fXX[962] = 0.0;
  fXX[72] = 0.0;
  fXX[172] = 0.0;
  fXX[272] = 0.0;
  fXX[372] = 0.0;
  fXX[472] = 0.0;
  fXX[572] = 0.0;
  fXX[672] = 0.0;
  fXX[772] = 0.0;
  fXX[872] = 0.0;
  fXX[972] = 0.0;
  fXX[82] = 0.0;
  fXX[182] = 0.0;
  fXX[282] = 0.0;
  fXX[382] = 0.0;
  fXX[482] = 0.0;
  fXX[582] = 0.0;
  fXX[682] = 0.0;
  fXX[782] = 0.0;
  fXX[882] = 0.0;
  fXX[982] = 0.0;
  fXX[92] = 0.0;
  fXX[192] = 0.0;
  fXX[292] = 0.0;
  fXX[392] = 0.0;
  fXX[492] = 0.0;
  fXX[592] = 0.0;
  fXX[692] = 0.0;
  fXX[792] = 0.0;
  fXX[892] = 0.0;
  fXX[992] = 0.0;
  st.site = &mb_emlrtRSI;
  a_tmp = mu + X[0];
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mb_emlrtRSI;
  b_a_tmp = X[1] * X[1];
  c_a_tmp = X[2] * X[2];
  a = ((a_tmp - 1.0) * (a_tmp - 1.0) + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &mb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mb_emlrtRSI;
  b_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &mb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mb_emlrtRSI;
  c_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &mb_emlrtRSI;
  d_a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mb_emlrtRSI;
  d_a = (d_a * d_a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (d_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &mb_emlrtRSI;
  d_a_tmp = 2.0 * mu + 2.0 * X[0];
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mb_emlrtRSI;
  e_a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mb_emlrtRSI;
  e_a = (e_a * e_a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (e_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &mb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mb_emlrtRSI;
  f_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (f_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &mb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mb_emlrtRSI;
  g_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (g_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &mb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mb_emlrtRSI;
  h_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (h_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &mb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mb_emlrtRSI;
  i_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (i_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fXX_tmp = 3.0 * A_sc * C_R * Phi;
  b_fXX_tmp = c_light * X[6];
  c_fXX_tmp = 15.0 * A_sc * C_R * Phi;
  d_fXX_tmp = fXX_tmp * d_a_tmp;
  e_fXX_tmp = 3.0 * mu * ((mu + X[0]) - 1.0);
  f_fXX_tmp = 3.0 * a_tmp * (mu - 1.0);
  g_fXX_tmp = fXX_tmp * a_tmp;
  h_fXX_tmp = 4.0 * c_light * X[6];
  i_fXX_tmp = 3.0 * mu * ((2.0 * mu + 2.0 * X[0]) - 2.0);
  j_fXX_tmp = 3.0 * d_a_tmp * (mu - 1.0);
  fXX[3] = (((((((e_fXX_tmp / muDoubleScalarPower(a, 2.5) - j_fXX_tmp /
                  muDoubleScalarPower(b_a, 2.5)) - f_fXX_tmp /
                 muDoubleScalarPower(c_a, 2.5)) + i_fXX_tmp /
                muDoubleScalarPower(d_a, 2.5)) - 15.0 * mu * ((d_a_tmp - 2.0) *
    (d_a_tmp - 2.0)) * ((mu + X[0]) - 1.0) / (4.0 * muDoubleScalarPower(e_a, 3.5)))
              + 15.0 * (d_a_tmp * d_a_tmp) * a_tmp * (mu - 1.0) / (4.0 *
    muDoubleScalarPower(f_a, 3.5))) - g_fXX_tmp / (b_fXX_tmp *
              muDoubleScalarPower(g_a, 2.5))) - d_fXX_tmp / (b_fXX_tmp *
             muDoubleScalarPower(h_a, 2.5))) + c_fXX_tmp * (d_a_tmp * d_a_tmp) *
    a_tmp / (h_fXX_tmp * muDoubleScalarPower(i_a, 3.5));
  st.site = &nb_emlrtRSI;
  a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &nb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &nb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &nb_emlrtRSI;
  a = (a * a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &nb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &nb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &nb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &nb_emlrtRSI;
  b_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &nb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &nb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &nb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &nb_emlrtRSI;
  c_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &nb_emlrtRSI;
  d_a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &nb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &nb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &nb_emlrtRSI;
  d_a = (d_a * d_a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (d_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &nb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &nb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &nb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &nb_emlrtRSI;
  e_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (e_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &nb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &nb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &nb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &nb_emlrtRSI;
  f_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (f_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  k_fXX_tmp = 2.0 * c_light * X[6];
  l_fXX_tmp = 3.0 * mu * X[1];
  m_fXX_tmp = 3.0 * X[1] * (mu - 1.0);
  n_fXX_tmp = 15.0 * X[1] * d_a_tmp * a_tmp * (mu - 1.0);
  fXX_tmp_tmp = 15.0 * mu * X[1];
  o_fXX_tmp = fXX_tmp_tmp * ((2.0 * mu + 2.0 * X[0]) - 2.0) * ((mu + X[0]) - 1.0);
  p_fXX_tmp = fXX_tmp * X[1];
  b_fXX_tmp_tmp = c_fXX_tmp * X[1];
  q_fXX_tmp = b_fXX_tmp_tmp * d_a_tmp * a_tmp;
  fXX[103] = ((((l_fXX_tmp / muDoubleScalarPower(a, 2.5) - m_fXX_tmp /
                 muDoubleScalarPower(b_a, 2.5)) + n_fXX_tmp / (2.0 *
    muDoubleScalarPower(c_a, 3.5))) - o_fXX_tmp / (2.0 * muDoubleScalarPower(d_a,
    3.5))) - p_fXX_tmp / (b_fXX_tmp * muDoubleScalarPower(e_a, 2.5))) +
    q_fXX_tmp / (k_fXX_tmp * muDoubleScalarPower(f_a, 3.5));
  st.site = &ob_emlrtRSI;
  a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ob_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ob_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ob_emlrtRSI;
  a = (a * a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &ob_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ob_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ob_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ob_emlrtRSI;
  b_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &ob_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ob_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ob_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ob_emlrtRSI;
  c_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &ob_emlrtRSI;
  d_a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ob_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ob_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ob_emlrtRSI;
  d_a = (d_a * d_a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (d_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &ob_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ob_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ob_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ob_emlrtRSI;
  e_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (e_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &ob_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ob_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ob_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ob_emlrtRSI;
  f_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (f_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  r_fXX_tmp = 3.0 * mu * X[2];
  s_fXX_tmp = 3.0 * X[2] * (mu - 1.0);
  t_fXX_tmp = 15.0 * X[2] * d_a_tmp * a_tmp * (mu - 1.0);
  c_fXX_tmp_tmp = 15.0 * mu * X[2];
  u_fXX_tmp = c_fXX_tmp_tmp * ((2.0 * mu + 2.0 * X[0]) - 2.0) * ((mu + X[0]) -
    1.0);
  v_fXX_tmp = fXX_tmp * X[2];
  d_fXX_tmp_tmp = c_fXX_tmp * X[2];
  w_fXX_tmp = d_fXX_tmp_tmp * d_a_tmp * a_tmp;
  fXX[203] = ((((r_fXX_tmp / muDoubleScalarPower(a, 2.5) - s_fXX_tmp /
                 muDoubleScalarPower(b_a, 2.5)) + t_fXX_tmp / (2.0 *
    muDoubleScalarPower(c_a, 3.5))) - u_fXX_tmp / (2.0 * muDoubleScalarPower(d_a,
    3.5))) - v_fXX_tmp / (b_fXX_tmp * muDoubleScalarPower(e_a, 2.5))) +
    w_fXX_tmp / (k_fXX_tmp * muDoubleScalarPower(f_a, 3.5));
  fXX[303] = 0.0;
  fXX[403] = 0.0;
  fXX[503] = 0.0;
  st.site = &pb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &pb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &pb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &pb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &pb_emlrtRSI;
  a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &pb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &pb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &pb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &pb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &pb_emlrtRSI;
  b_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  x_fXX_tmp = X[6] * X[6];
  y_fXX_tmp = c_light * x_fXX_tmp;
  ab_fXX_tmp = 2.0 * c_light * x_fXX_tmp;
  g_a = d_fXX_tmp * a_tmp;
  bb_fXX_tmp = A_sc * C_R * Phi;
  fXX[603] = g_a / (ab_fXX_tmp * muDoubleScalarPower(a, 2.5)) - bb_fXX_tmp /
    (y_fXX_tmp * muDoubleScalarPower(b_a, 1.5));
  fXX[703] = 0.0;
  fXX[803] = 0.0;
  fXX[903] = 0.0;
  st.site = &qb_emlrtRSI;
  a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &qb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &qb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &qb_emlrtRSI;
  a = (a * a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &qb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &qb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &qb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &qb_emlrtRSI;
  b_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &qb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &qb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &qb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &qb_emlrtRSI;
  c_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &qb_emlrtRSI;
  d_a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &qb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &qb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &qb_emlrtRSI;
  d_a = (d_a * d_a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (d_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &qb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &qb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &qb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &qb_emlrtRSI;
  e_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (e_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &qb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &qb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &qb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &qb_emlrtRSI;
  f_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (f_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fXX[13] = ((((l_fXX_tmp / muDoubleScalarPower(a, 2.5) - m_fXX_tmp /
                muDoubleScalarPower(b_a, 2.5)) + n_fXX_tmp / (2.0 *
    muDoubleScalarPower(c_a, 3.5))) - o_fXX_tmp / (2.0 * muDoubleScalarPower(d_a,
    3.5))) - p_fXX_tmp / (b_fXX_tmp * muDoubleScalarPower(e_a, 2.5))) +
    q_fXX_tmp / (k_fXX_tmp * muDoubleScalarPower(f_a, 3.5));
  st.site = &rb_emlrtRSI;
  a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &rb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &rb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &rb_emlrtRSI;
  a = (a * a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &rb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &rb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &rb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &rb_emlrtRSI;
  b_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &rb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &rb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &rb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &rb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &rb_emlrtRSI;
  c_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &rb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &rb_emlrtRSI;
  d_a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &rb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &rb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &rb_emlrtRSI;
  d_a = (d_a * d_a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (d_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &rb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &rb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &rb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &rb_emlrtRSI;
  e_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (e_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &rb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &rb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &rb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &rb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &rb_emlrtRSI;
  f_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (f_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  n_fXX_tmp = 15.0 * b_a_tmp;
  o_fXX_tmp = 15.0 * mu * b_a_tmp;
  q_fXX_tmp = c_fXX_tmp * b_a_tmp;
  fXX[113] = ((((e_fXX_tmp / muDoubleScalarPower(a, 2.5) - f_fXX_tmp /
                 muDoubleScalarPower(b_a, 2.5)) + n_fXX_tmp * a_tmp * (mu - 1.0)
                / muDoubleScalarPower(c_a, 3.5)) - o_fXX_tmp * ((mu + X[0]) -
    1.0) / muDoubleScalarPower(d_a, 3.5)) - g_fXX_tmp / (b_fXX_tmp *
    muDoubleScalarPower(e_a, 2.5))) + q_fXX_tmp * a_tmp / (b_fXX_tmp *
    muDoubleScalarPower(f_a, 3.5));
  st.site = &sb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &sb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &sb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &sb_emlrtRSI;
  a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &sb_emlrtRSI;
  b_a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &sb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &sb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &sb_emlrtRSI;
  b_a = (b_a * b_a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &sb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &sb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &sb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &sb_emlrtRSI;
  c_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  e_fXX_tmp_tmp = 15.0 * X[1] * X[2];
  cb_fXX_tmp = e_fXX_tmp_tmp * a_tmp * (mu - 1.0);
  h_a = fXX_tmp_tmp * X[2];
  db_fXX_tmp = h_a * ((mu + X[0]) - 1.0);
  i_a = b_fXX_tmp_tmp * X[2];
  eb_fXX_tmp = i_a * a_tmp;
  fXX[213] = (cb_fXX_tmp / muDoubleScalarPower(a, 3.5) - db_fXX_tmp /
              muDoubleScalarPower(b_a, 3.5)) + eb_fXX_tmp / (b_fXX_tmp *
    muDoubleScalarPower(c_a, 3.5));
  fXX[313] = 0.0;
  fXX[413] = 0.0;
  fXX[513] = 0.0;
  st.site = &tb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &tb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &tb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &tb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &tb_emlrtRSI;
  a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fb_fXX_tmp = p_fXX_tmp * a_tmp;
  fXX[613] = fb_fXX_tmp / (y_fXX_tmp * muDoubleScalarPower(a, 2.5));
  fXX[713] = 0.0;
  fXX[813] = 0.0;
  fXX[913] = 0.0;
  st.site = &ub_emlrtRSI;
  a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ub_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ub_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ub_emlrtRSI;
  a = (a * a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &ub_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ub_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ub_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ub_emlrtRSI;
  b_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &ub_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ub_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ub_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ub_emlrtRSI;
  c_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &ub_emlrtRSI;
  d_a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ub_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ub_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ub_emlrtRSI;
  d_a = (d_a * d_a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (d_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &ub_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ub_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ub_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ub_emlrtRSI;
  e_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (e_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &ub_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ub_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ub_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ub_emlrtRSI;
  f_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (f_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fXX[23] = ((((r_fXX_tmp / muDoubleScalarPower(a, 2.5) - s_fXX_tmp /
                muDoubleScalarPower(b_a, 2.5)) + t_fXX_tmp / (2.0 *
    muDoubleScalarPower(c_a, 3.5))) - u_fXX_tmp / (2.0 * muDoubleScalarPower(d_a,
    3.5))) - v_fXX_tmp / (b_fXX_tmp * muDoubleScalarPower(e_a, 2.5))) +
    w_fXX_tmp / (k_fXX_tmp * muDoubleScalarPower(f_a, 3.5));
  st.site = &vb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &vb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &vb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &vb_emlrtRSI;
  a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &vb_emlrtRSI;
  b_a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &vb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &vb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &vb_emlrtRSI;
  b_a = (b_a * b_a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &vb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &vb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &vb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &vb_emlrtRSI;
  c_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fXX[123] = (cb_fXX_tmp / muDoubleScalarPower(a, 3.5) - db_fXX_tmp /
              muDoubleScalarPower(b_a, 3.5)) + eb_fXX_tmp / (b_fXX_tmp *
    muDoubleScalarPower(c_a, 3.5));
  st.site = &wb_emlrtRSI;
  a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &wb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &wb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &wb_emlrtRSI;
  a = (a * a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &wb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &wb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &wb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &wb_emlrtRSI;
  b_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &wb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &wb_emlrtRSI;
  c_a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &wb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &wb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &wb_emlrtRSI;
  c_a = (c_a * c_a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &wb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &wb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &wb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &wb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &wb_emlrtRSI;
  d_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (d_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &wb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &wb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &wb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &wb_emlrtRSI;
  e_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (e_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &wb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &wb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &wb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &wb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &wb_emlrtRSI;
  f_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (f_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  t_fXX_tmp = 15.0 * c_a_tmp;
  u_fXX_tmp = 15.0 * mu * c_a_tmp;
  w_fXX_tmp = c_fXX_tmp * c_a_tmp;
  fXX[223] = ((((e_fXX_tmp / muDoubleScalarPower(a, 2.5) - f_fXX_tmp /
                 muDoubleScalarPower(b_a, 2.5)) - u_fXX_tmp * ((mu + X[0]) - 1.0)
                / muDoubleScalarPower(c_a, 3.5)) + t_fXX_tmp * a_tmp * (mu - 1.0)
               / muDoubleScalarPower(d_a, 3.5)) - g_fXX_tmp / (b_fXX_tmp *
    muDoubleScalarPower(e_a, 2.5))) + w_fXX_tmp * a_tmp / (b_fXX_tmp *
    muDoubleScalarPower(f_a, 3.5));
  fXX[323] = 0.0;
  fXX[423] = 0.0;
  fXX[523] = 0.0;
  st.site = &xb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &xb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &xb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &xb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &xb_emlrtRSI;
  a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  e_fXX_tmp = v_fXX_tmp * a_tmp;
  fXX[623] = e_fXX_tmp / (y_fXX_tmp * muDoubleScalarPower(a, 2.5));
  fXX[723] = 0.0;
  fXX[823] = 0.0;
  fXX[923] = 0.0;
  fXX[33] = 0.0;
  fXX[133] = 0.0;
  fXX[233] = 0.0;
  fXX[333] = 0.0;
  fXX[433] = 0.0;
  fXX[533] = 0.0;
  fXX[633] = 0.0;
  fXX[733] = 0.0;
  fXX[833] = 0.0;
  fXX[933] = 0.0;
  fXX[43] = 0.0;
  fXX[143] = 0.0;
  fXX[243] = 0.0;
  fXX[343] = 0.0;
  fXX[443] = 0.0;
  fXX[543] = 0.0;
  fXX[643] = 0.0;
  fXX[743] = 0.0;
  fXX[843] = 0.0;
  fXX[943] = 0.0;
  fXX[53] = 0.0;
  fXX[153] = 0.0;
  fXX[253] = 0.0;
  fXX[353] = 0.0;
  fXX[453] = 0.0;
  fXX[553] = 0.0;
  fXX[653] = 0.0;
  fXX[753] = 0.0;
  fXX[853] = 0.0;
  fXX[953] = 0.0;
  st.site = &yb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &yb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &yb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &yb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &yb_emlrtRSI;
  a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &yb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &yb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &yb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &yb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &yb_emlrtRSI;
  b_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fXX[63] = g_a / (ab_fXX_tmp * muDoubleScalarPower(a, 2.5)) - bb_fXX_tmp /
    (y_fXX_tmp * muDoubleScalarPower(b_a, 1.5));
  st.site = &ac_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ac_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ac_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ac_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ac_emlrtRSI;
  a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fXX[163] = fb_fXX_tmp / (y_fXX_tmp * muDoubleScalarPower(a, 2.5));
  st.site = &bc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &bc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &bc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &bc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &bc_emlrtRSI;
  a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fXX[263] = e_fXX_tmp / (y_fXX_tmp * muDoubleScalarPower(a, 2.5));
  fXX[363] = 0.0;
  fXX[463] = 0.0;
  fXX[563] = 0.0;
  st.site = &cc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &cc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &cc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &cc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &cc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &cc_emlrtRSI;
  a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  e_fXX_tmp = muDoubleScalarPower(X[6], 3.0);
  f_fXX_tmp = 2.0 * A_sc * C_R * Phi;
  g_fXX_tmp = c_light * e_fXX_tmp;
  fXX[663] = 2.0 * Tmax * X[7] / e_fXX_tmp + f_fXX_tmp * a_tmp / (g_fXX_tmp *
    muDoubleScalarPower(a, 1.5));
  st.site = &dc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  x_fXX_tmp = -Tmax / x_fXX_tmp;
  fXX[763] = x_fXX_tmp;
  fXX[863] = 0.0;
  fXX[963] = 0.0;
  fXX[73] = 0.0;
  fXX[173] = 0.0;
  fXX[273] = 0.0;
  fXX[373] = 0.0;
  fXX[473] = 0.0;
  fXX[573] = 0.0;
  st.site = &ec_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  fXX[673] = x_fXX_tmp;
  fXX[773] = 0.0;
  fXX[873] = 0.0;
  fXX[973] = 0.0;
  fXX[83] = 0.0;
  fXX[183] = 0.0;
  fXX[283] = 0.0;
  fXX[383] = 0.0;
  fXX[483] = 0.0;
  fXX[583] = 0.0;
  fXX[683] = 0.0;
  fXX[783] = 0.0;
  fXX[883] = 0.0;
  fXX[983] = 0.0;
  fXX[93] = 0.0;
  fXX[193] = 0.0;
  fXX[293] = 0.0;
  fXX[393] = 0.0;
  fXX[493] = 0.0;
  fXX[593] = 0.0;
  fXX[693] = 0.0;
  fXX[793] = 0.0;
  fXX[893] = 0.0;
  fXX[993] = 0.0;
  st.site = &fc_emlrtRSI;
  a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &fc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &fc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &fc_emlrtRSI;
  a = (a * a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &fc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &fc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &fc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &fc_emlrtRSI;
  b_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &fc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &fc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &fc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &fc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &fc_emlrtRSI;
  c_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &fc_emlrtRSI;
  d_a = (2.0 * mu + 2.0 * X[0]) - 2.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &fc_emlrtRSI;
  e_a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &fc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &fc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &fc_emlrtRSI;
  e_a = (e_a * e_a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (e_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &fc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &fc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &fc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &fc_emlrtRSI;
  f_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (f_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &fc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &fc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &fc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &fc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &fc_emlrtRSI;
  g_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (g_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fXX[4] = ((((l_fXX_tmp / muDoubleScalarPower(a, 2.5) - m_fXX_tmp /
               muDoubleScalarPower(b_a, 2.5)) + 15.0 * X[1] * (d_a_tmp * d_a_tmp)
              * (mu - 1.0) / (4.0 * muDoubleScalarPower(c_a, 3.5))) -
             fXX_tmp_tmp * (d_a * d_a) / (4.0 * muDoubleScalarPower(e_a, 3.5)))
            - p_fXX_tmp / (b_fXX_tmp * muDoubleScalarPower(f_a, 2.5))) +
    b_fXX_tmp_tmp * (d_a_tmp * d_a_tmp) / (h_fXX_tmp * muDoubleScalarPower(g_a,
    3.5));
  st.site = &gc_emlrtRSI;
  a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gc_emlrtRSI;
  a = (a * a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &gc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gc_emlrtRSI;
  b_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &gc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gc_emlrtRSI;
  c_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &gc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gc_emlrtRSI;
  d_a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gc_emlrtRSI;
  d_a = (d_a * d_a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (d_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &gc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gc_emlrtRSI;
  e_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (e_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &gc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gc_emlrtRSI;
  f_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (f_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  g_a = n_fXX_tmp * d_a_tmp * (mu - 1.0);
  cb_fXX_tmp = o_fXX_tmp * ((2.0 * mu + 2.0 * X[0]) - 2.0);
  db_fXX_tmp = q_fXX_tmp * d_a_tmp;
  fXX[104] = ((((i_fXX_tmp / (2.0 * muDoubleScalarPower(a, 2.5)) - j_fXX_tmp /
                 (2.0 * muDoubleScalarPower(b_a, 2.5))) + g_a / (2.0 *
    muDoubleScalarPower(c_a, 3.5))) - cb_fXX_tmp / (2.0 * muDoubleScalarPower
    (d_a, 3.5))) - d_fXX_tmp / (k_fXX_tmp * muDoubleScalarPower(e_a, 2.5))) +
    db_fXX_tmp / (k_fXX_tmp * muDoubleScalarPower(f_a, 3.5));
  st.site = &hc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &hc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &hc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &hc_emlrtRSI;
  a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &hc_emlrtRSI;
  b_a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &hc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &hc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &hc_emlrtRSI;
  b_a = (b_a * b_a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &hc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &hc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &hc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &hc_emlrtRSI;
  c_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  eb_fXX_tmp = e_fXX_tmp_tmp * d_a_tmp * (mu - 1.0);
  fb_fXX_tmp = h_a * ((2.0 * mu + 2.0 * X[0]) - 2.0);
  gb_fXX_tmp = i_a * d_a_tmp;
  fXX[204] = (eb_fXX_tmp / (2.0 * muDoubleScalarPower(a, 3.5)) - fb_fXX_tmp /
              (2.0 * muDoubleScalarPower(b_a, 3.5))) + gb_fXX_tmp / (k_fXX_tmp *
    muDoubleScalarPower(c_a, 3.5));
  fXX[304] = 0.0;
  fXX[404] = 0.0;
  fXX[504] = 0.0;
  st.site = &ic_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ic_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ic_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ic_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ic_emlrtRSI;
  a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  e_fXX_tmp_tmp = p_fXX_tmp * d_a_tmp;
  fXX[604] = e_fXX_tmp_tmp / (ab_fXX_tmp * muDoubleScalarPower(a, 2.5));
  fXX[704] = 0.0;
  fXX[804] = 0.0;
  fXX[904] = 0.0;
  st.site = &jc_emlrtRSI;
  a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &jc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &jc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &jc_emlrtRSI;
  a = (a * a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &jc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &jc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &jc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &jc_emlrtRSI;
  b_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &jc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &jc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &jc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &jc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &jc_emlrtRSI;
  c_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &jc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &jc_emlrtRSI;
  d_a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &jc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &jc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &jc_emlrtRSI;
  d_a = (d_a * d_a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (d_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &jc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &jc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &jc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &jc_emlrtRSI;
  e_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (e_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &jc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &jc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &jc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &jc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &jc_emlrtRSI;
  f_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (f_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fXX[14] = ((((i_fXX_tmp / (2.0 * muDoubleScalarPower(a, 2.5)) - j_fXX_tmp /
                (2.0 * muDoubleScalarPower(b_a, 2.5))) + g_a / (2.0 *
    muDoubleScalarPower(c_a, 3.5))) - cb_fXX_tmp / (2.0 * muDoubleScalarPower
    (d_a, 3.5))) - d_fXX_tmp / (k_fXX_tmp * muDoubleScalarPower(e_a, 2.5))) +
    db_fXX_tmp / (k_fXX_tmp * muDoubleScalarPower(f_a, 3.5));
  st.site = &kc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &kc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &kc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &kc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &kc_emlrtRSI;
  a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &kc_emlrtRSI;
  b_a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &kc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &kc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &kc_emlrtRSI;
  b_a = (b_a * b_a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &kc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &kc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &kc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &kc_emlrtRSI;
  c_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &kc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &kc_emlrtRSI;
  d_a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &kc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &kc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &kc_emlrtRSI;
  d_a = (d_a * d_a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (d_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &kc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &kc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &kc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &kc_emlrtRSI;
  e_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (e_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &kc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &kc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &kc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &kc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &kc_emlrtRSI;
  f_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (f_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  g_a = muDoubleScalarPower(X[1], 3.0);
  cb_fXX_tmp = 9.0 * A_sc * C_R * Phi;
  fXX[114] = ((((15.0 * g_a * (mu - 1.0) / muDoubleScalarPower(a, 3.5) + 9.0 *
                 mu * X[1] / muDoubleScalarPower(b_a, 2.5)) - 9.0 * X[1] * (mu -
    1.0) / muDoubleScalarPower(c_a, 2.5)) - 15.0 * mu * g_a /
               muDoubleScalarPower(d_a, 3.5)) - cb_fXX_tmp * X[1] / (b_fXX_tmp *
    muDoubleScalarPower(e_a, 2.5))) + c_fXX_tmp * g_a / (b_fXX_tmp *
    muDoubleScalarPower(f_a, 3.5));
  st.site = &lc_emlrtRSI;
  a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &lc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &lc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &lc_emlrtRSI;
  a = (a * a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &lc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &lc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &lc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &lc_emlrtRSI;
  b_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &lc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &lc_emlrtRSI;
  c_a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &lc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &lc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &lc_emlrtRSI;
  c_a = (c_a * c_a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &lc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &lc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &lc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &lc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &lc_emlrtRSI;
  d_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (d_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &lc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &lc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &lc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &lc_emlrtRSI;
  e_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (e_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &lc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &lc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &lc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &lc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &lc_emlrtRSI;
  f_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (f_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  o_fXX_tmp *= X[2];
  n_fXX_tmp = n_fXX_tmp * X[2] * (mu - 1.0);
  q_fXX_tmp *= X[2];
  fXX[214] = ((((r_fXX_tmp / muDoubleScalarPower(a, 2.5) - s_fXX_tmp /
                 muDoubleScalarPower(b_a, 2.5)) - o_fXX_tmp /
                muDoubleScalarPower(c_a, 3.5)) + n_fXX_tmp / muDoubleScalarPower
               (d_a, 3.5)) - v_fXX_tmp / (b_fXX_tmp * muDoubleScalarPower(e_a,
    2.5))) + q_fXX_tmp / (b_fXX_tmp * muDoubleScalarPower(f_a, 3.5));
  fXX[314] = 0.0;
  fXX[414] = 0.0;
  fXX[514] = 0.0;
  st.site = &mc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mc_emlrtRSI;
  a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &mc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &mc_emlrtRSI;
  b_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  g_a = fXX_tmp * b_a_tmp;
  fXX[614] = g_a / (y_fXX_tmp * muDoubleScalarPower(a, 2.5)) - bb_fXX_tmp /
    (y_fXX_tmp * muDoubleScalarPower(b_a, 1.5));
  fXX[714] = 0.0;
  fXX[814] = 0.0;
  fXX[914] = 0.0;
  st.site = &nc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &nc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &nc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &nc_emlrtRSI;
  a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &nc_emlrtRSI;
  b_a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &nc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &nc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &nc_emlrtRSI;
  b_a = (b_a * b_a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &nc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &nc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &nc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &nc_emlrtRSI;
  c_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fXX[24] = (eb_fXX_tmp / (2.0 * muDoubleScalarPower(a, 3.5)) - fb_fXX_tmp /
             (2.0 * muDoubleScalarPower(b_a, 3.5))) + gb_fXX_tmp / (k_fXX_tmp *
    muDoubleScalarPower(c_a, 3.5));
  st.site = &oc_emlrtRSI;
  a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &oc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &oc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &oc_emlrtRSI;
  a = (a * a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &oc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &oc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &oc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &oc_emlrtRSI;
  b_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &oc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &oc_emlrtRSI;
  c_a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &oc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &oc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &oc_emlrtRSI;
  c_a = (c_a * c_a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &oc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &oc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &oc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &oc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &oc_emlrtRSI;
  d_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (d_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &oc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &oc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &oc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &oc_emlrtRSI;
  e_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (e_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &oc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &oc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &oc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &oc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &oc_emlrtRSI;
  f_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (f_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fXX[124] = ((((r_fXX_tmp / muDoubleScalarPower(a, 2.5) - s_fXX_tmp /
                 muDoubleScalarPower(b_a, 2.5)) - o_fXX_tmp /
                muDoubleScalarPower(c_a, 3.5)) + n_fXX_tmp / muDoubleScalarPower
               (d_a, 3.5)) - v_fXX_tmp / (b_fXX_tmp * muDoubleScalarPower(e_a,
    2.5))) + q_fXX_tmp / (b_fXX_tmp * muDoubleScalarPower(f_a, 3.5));
  st.site = &pc_emlrtRSI;
  a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &pc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &pc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &pc_emlrtRSI;
  a = (a * a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &pc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &pc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &pc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &pc_emlrtRSI;
  b_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &pc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &pc_emlrtRSI;
  c_a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &pc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &pc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &pc_emlrtRSI;
  c_a = (c_a * c_a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &pc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &pc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &pc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &pc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &pc_emlrtRSI;
  d_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (d_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &pc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &pc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &pc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &pc_emlrtRSI;
  e_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (e_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &pc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &pc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &pc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &pc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &pc_emlrtRSI;
  f_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (f_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  db_fXX_tmp = fXX_tmp_tmp * c_a_tmp;
  fXX_tmp_tmp = 15.0 * X[1] * c_a_tmp * (mu - 1.0);
  h_a = b_fXX_tmp_tmp * c_a_tmp;
  fXX[224] = ((((l_fXX_tmp / muDoubleScalarPower(a, 2.5) - m_fXX_tmp /
                 muDoubleScalarPower(b_a, 2.5)) - db_fXX_tmp /
                muDoubleScalarPower(c_a, 3.5)) + fXX_tmp_tmp /
               muDoubleScalarPower(d_a, 3.5)) - p_fXX_tmp / (b_fXX_tmp *
    muDoubleScalarPower(e_a, 2.5))) + h_a / (b_fXX_tmp * muDoubleScalarPower(f_a,
    3.5));
  fXX[324] = 0.0;
  fXX[424] = 0.0;
  fXX[524] = 0.0;
  st.site = &qc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &qc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &qc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &qc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &qc_emlrtRSI;
  a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  i_a = p_fXX_tmp * X[2];
  fXX[624] = i_a / (y_fXX_tmp * muDoubleScalarPower(a, 2.5));
  fXX[724] = 0.0;
  fXX[824] = 0.0;
  fXX[924] = 0.0;
  fXX[34] = 0.0;
  fXX[134] = 0.0;
  fXX[234] = 0.0;
  fXX[334] = 0.0;
  fXX[434] = 0.0;
  fXX[534] = 0.0;
  fXX[634] = 0.0;
  fXX[734] = 0.0;
  fXX[834] = 0.0;
  fXX[934] = 0.0;
  fXX[44] = 0.0;
  fXX[144] = 0.0;
  fXX[244] = 0.0;
  fXX[344] = 0.0;
  fXX[444] = 0.0;
  fXX[544] = 0.0;
  fXX[644] = 0.0;
  fXX[744] = 0.0;
  fXX[844] = 0.0;
  fXX[944] = 0.0;
  fXX[54] = 0.0;
  fXX[154] = 0.0;
  fXX[254] = 0.0;
  fXX[354] = 0.0;
  fXX[454] = 0.0;
  fXX[554] = 0.0;
  fXX[654] = 0.0;
  fXX[754] = 0.0;
  fXX[854] = 0.0;
  fXX[954] = 0.0;
  st.site = &rc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &rc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &rc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &rc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &rc_emlrtRSI;
  a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fXX[64] = e_fXX_tmp_tmp / (ab_fXX_tmp * muDoubleScalarPower(a, 2.5));
  st.site = &sc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &sc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &sc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &sc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &sc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &sc_emlrtRSI;
  a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &sc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &sc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &sc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &sc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &sc_emlrtRSI;
  b_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fXX[164] = g_a / (y_fXX_tmp * muDoubleScalarPower(a, 2.5)) - bb_fXX_tmp /
    (y_fXX_tmp * muDoubleScalarPower(b_a, 1.5));
  st.site = &tc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &tc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &tc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &tc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &tc_emlrtRSI;
  a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fXX[264] = i_a / (y_fXX_tmp * muDoubleScalarPower(a, 2.5));
  fXX[364] = 0.0;
  fXX[464] = 0.0;
  fXX[564] = 0.0;
  st.site = &uc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &uc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &uc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &uc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &uc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &uc_emlrtRSI;
  a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fXX[664] = 2.0 * Tmax * X[8] / e_fXX_tmp + f_fXX_tmp * X[1] / (g_fXX_tmp *
    muDoubleScalarPower(a, 1.5));
  fXX[764] = 0.0;
  st.site = &vc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  fXX[864] = x_fXX_tmp;
  fXX[964] = 0.0;
  fXX[74] = 0.0;
  fXX[174] = 0.0;
  fXX[274] = 0.0;
  fXX[374] = 0.0;
  fXX[474] = 0.0;
  fXX[574] = 0.0;
  fXX[674] = 0.0;
  fXX[774] = 0.0;
  fXX[874] = 0.0;
  fXX[974] = 0.0;
  fXX[84] = 0.0;
  fXX[184] = 0.0;
  fXX[284] = 0.0;
  fXX[384] = 0.0;
  fXX[484] = 0.0;
  fXX[584] = 0.0;
  st.site = &wc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  fXX[684] = x_fXX_tmp;
  fXX[784] = 0.0;
  fXX[884] = 0.0;
  fXX[984] = 0.0;
  fXX[94] = 0.0;
  fXX[194] = 0.0;
  fXX[294] = 0.0;
  fXX[394] = 0.0;
  fXX[494] = 0.0;
  fXX[594] = 0.0;
  fXX[694] = 0.0;
  fXX[794] = 0.0;
  fXX[894] = 0.0;
  fXX[994] = 0.0;
  st.site = &xc_emlrtRSI;
  a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &xc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &xc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &xc_emlrtRSI;
  a = (a * a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &xc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &xc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &xc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &xc_emlrtRSI;
  b_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &xc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &xc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &xc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &xc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &xc_emlrtRSI;
  c_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &xc_emlrtRSI;
  d_a = (2.0 * mu + 2.0 * X[0]) - 2.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &xc_emlrtRSI;
  e_a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &xc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &xc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &xc_emlrtRSI;
  e_a = (e_a * e_a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (e_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &xc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &xc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &xc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &xc_emlrtRSI;
  f_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (f_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &xc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &xc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &xc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &xc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &xc_emlrtRSI;
  g_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (g_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fXX[5] = ((((r_fXX_tmp / muDoubleScalarPower(a, 2.5) - s_fXX_tmp /
               muDoubleScalarPower(b_a, 2.5)) + 15.0 * X[2] * (d_a_tmp * d_a_tmp)
              * (mu - 1.0) / (4.0 * muDoubleScalarPower(c_a, 3.5))) -
             c_fXX_tmp_tmp * (d_a * d_a) / (4.0 * muDoubleScalarPower(e_a, 3.5)))
            - v_fXX_tmp / (b_fXX_tmp * muDoubleScalarPower(f_a, 2.5))) +
    d_fXX_tmp_tmp * (d_a_tmp * d_a_tmp) / (h_fXX_tmp * muDoubleScalarPower(g_a,
    3.5));
  st.site = &yc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &yc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &yc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &yc_emlrtRSI;
  a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &yc_emlrtRSI;
  b_a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &yc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &yc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &yc_emlrtRSI;
  b_a = (b_a * b_a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &yc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &yc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &yc_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &yc_emlrtRSI;
  c_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fXX[105] = (eb_fXX_tmp / (2.0 * muDoubleScalarPower(a, 3.5)) - fb_fXX_tmp /
              (2.0 * muDoubleScalarPower(b_a, 3.5))) + gb_fXX_tmp / (k_fXX_tmp *
    muDoubleScalarPower(c_a, 3.5));
  st.site = &ad_emlrtRSI;
  a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ad_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ad_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ad_emlrtRSI;
  a = (a * a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &ad_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ad_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ad_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ad_emlrtRSI;
  b_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &ad_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ad_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ad_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ad_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ad_emlrtRSI;
  c_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &ad_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ad_emlrtRSI;
  d_a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ad_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ad_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ad_emlrtRSI;
  d_a = (d_a * d_a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (d_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &ad_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ad_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ad_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ad_emlrtRSI;
  e_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (e_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &ad_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ad_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ad_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ad_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ad_emlrtRSI;
  f_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (f_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  h_fXX_tmp = t_fXX_tmp * d_a_tmp * (mu - 1.0);
  t_fXX_tmp = u_fXX_tmp * ((2.0 * mu + 2.0 * X[0]) - 2.0);
  u_fXX_tmp = w_fXX_tmp * d_a_tmp;
  fXX[205] = ((((i_fXX_tmp / (2.0 * muDoubleScalarPower(a, 2.5)) - j_fXX_tmp /
                 (2.0 * muDoubleScalarPower(b_a, 2.5))) + h_fXX_tmp / (2.0 *
    muDoubleScalarPower(c_a, 3.5))) - t_fXX_tmp / (2.0 * muDoubleScalarPower(d_a,
    3.5))) - d_fXX_tmp / (k_fXX_tmp * muDoubleScalarPower(e_a, 2.5))) +
    u_fXX_tmp / (k_fXX_tmp * muDoubleScalarPower(f_a, 3.5));
  fXX[305] = 0.0;
  fXX[405] = 0.0;
  fXX[505] = 0.0;
  st.site = &bd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &bd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &bd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &bd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &bd_emlrtRSI;
  a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  w_fXX_tmp = v_fXX_tmp * d_a_tmp;
  fXX[605] = w_fXX_tmp / (ab_fXX_tmp * muDoubleScalarPower(a, 2.5));
  fXX[705] = 0.0;
  fXX[805] = 0.0;
  fXX[905] = 0.0;
  st.site = &cd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &cd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &cd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &cd_emlrtRSI;
  a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &cd_emlrtRSI;
  b_a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &cd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &cd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &cd_emlrtRSI;
  b_a = (b_a * b_a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &cd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &cd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &cd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &cd_emlrtRSI;
  c_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fXX[15] = (eb_fXX_tmp / (2.0 * muDoubleScalarPower(a, 3.5)) - fb_fXX_tmp /
             (2.0 * muDoubleScalarPower(b_a, 3.5))) + gb_fXX_tmp / (k_fXX_tmp *
    muDoubleScalarPower(c_a, 3.5));
  st.site = &dd_emlrtRSI;
  a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &dd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &dd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &dd_emlrtRSI;
  a = (a * a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &dd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &dd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &dd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &dd_emlrtRSI;
  b_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &dd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &dd_emlrtRSI;
  c_a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &dd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &dd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &dd_emlrtRSI;
  c_a = (c_a * c_a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &dd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &dd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &dd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &dd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &dd_emlrtRSI;
  d_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (d_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &dd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &dd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &dd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &dd_emlrtRSI;
  e_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (e_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &dd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &dd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &dd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &dd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &dd_emlrtRSI;
  f_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (f_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fXX[115] = ((((r_fXX_tmp / muDoubleScalarPower(a, 2.5) - s_fXX_tmp /
                 muDoubleScalarPower(b_a, 2.5)) - o_fXX_tmp /
                muDoubleScalarPower(c_a, 3.5)) + n_fXX_tmp / muDoubleScalarPower
               (d_a, 3.5)) - v_fXX_tmp / (b_fXX_tmp * muDoubleScalarPower(e_a,
    2.5))) + q_fXX_tmp / (b_fXX_tmp * muDoubleScalarPower(f_a, 3.5));
  st.site = &ed_emlrtRSI;
  a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ed_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ed_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ed_emlrtRSI;
  a = (a * a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &ed_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ed_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ed_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ed_emlrtRSI;
  b_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &ed_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ed_emlrtRSI;
  c_a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ed_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ed_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ed_emlrtRSI;
  c_a = (c_a * c_a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &ed_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ed_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ed_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ed_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ed_emlrtRSI;
  d_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (d_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &ed_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ed_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ed_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ed_emlrtRSI;
  e_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (e_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &ed_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ed_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ed_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ed_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ed_emlrtRSI;
  f_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (f_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fXX[215] = ((((l_fXX_tmp / muDoubleScalarPower(a, 2.5) - m_fXX_tmp /
                 muDoubleScalarPower(b_a, 2.5)) - db_fXX_tmp /
                muDoubleScalarPower(c_a, 3.5)) + fXX_tmp_tmp /
               muDoubleScalarPower(d_a, 3.5)) - p_fXX_tmp / (b_fXX_tmp *
    muDoubleScalarPower(e_a, 2.5))) + h_a / (b_fXX_tmp * muDoubleScalarPower(f_a,
    3.5));
  fXX[315] = 0.0;
  fXX[415] = 0.0;
  fXX[515] = 0.0;
  st.site = &fd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &fd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &fd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &fd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &fd_emlrtRSI;
  a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fXX[615] = i_a / (y_fXX_tmp * muDoubleScalarPower(a, 2.5));
  fXX[715] = 0.0;
  fXX[815] = 0.0;
  fXX[915] = 0.0;
  st.site = &gd_emlrtRSI;
  a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gd_emlrtRSI;
  a = (a * a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &gd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gd_emlrtRSI;
  b_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &gd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gd_emlrtRSI;
  c_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &gd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gd_emlrtRSI;
  d_a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gd_emlrtRSI;
  d_a = (d_a * d_a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (d_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &gd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gd_emlrtRSI;
  e_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (e_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &gd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &gd_emlrtRSI;
  f_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (f_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fXX[25] = ((((i_fXX_tmp / (2.0 * muDoubleScalarPower(a, 2.5)) - j_fXX_tmp /
                (2.0 * muDoubleScalarPower(b_a, 2.5))) + h_fXX_tmp / (2.0 *
    muDoubleScalarPower(c_a, 3.5))) - t_fXX_tmp / (2.0 * muDoubleScalarPower(d_a,
    3.5))) - d_fXX_tmp / (k_fXX_tmp * muDoubleScalarPower(e_a, 2.5))) +
    u_fXX_tmp / (k_fXX_tmp * muDoubleScalarPower(f_a, 3.5));
  st.site = &hd_emlrtRSI;
  a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &hd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &hd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &hd_emlrtRSI;
  a = (a * a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &hd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &hd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &hd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &hd_emlrtRSI;
  b_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &hd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &hd_emlrtRSI;
  c_a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &hd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &hd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &hd_emlrtRSI;
  c_a = (c_a * c_a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &hd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &hd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &hd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &hd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &hd_emlrtRSI;
  d_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (d_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &hd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &hd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &hd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &hd_emlrtRSI;
  e_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (e_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &hd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &hd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &hd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &hd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &hd_emlrtRSI;
  f_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (f_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fXX[125] = ((((l_fXX_tmp / muDoubleScalarPower(a, 2.5) - m_fXX_tmp /
                 muDoubleScalarPower(b_a, 2.5)) - db_fXX_tmp /
                muDoubleScalarPower(c_a, 3.5)) + fXX_tmp_tmp /
               muDoubleScalarPower(d_a, 3.5)) - p_fXX_tmp / (b_fXX_tmp *
    muDoubleScalarPower(e_a, 2.5))) + h_a / (b_fXX_tmp * muDoubleScalarPower(f_a,
    3.5));
  st.site = &id_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &id_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &id_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &id_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &id_emlrtRSI;
  a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &id_emlrtRSI;
  b_a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &id_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &id_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &id_emlrtRSI;
  b_a = (b_a * b_a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &id_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &id_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &id_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &id_emlrtRSI;
  c_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &id_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &id_emlrtRSI;
  d_a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &id_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &id_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &id_emlrtRSI;
  d_a = (d_a * d_a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (d_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &id_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &id_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &id_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &id_emlrtRSI;
  e_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (e_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &id_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &id_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &id_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &id_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &id_emlrtRSI;
  f_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (f_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  d_fXX_tmp = muDoubleScalarPower(X[2], 3.0);
  fXX[225] = ((((15.0 * d_fXX_tmp * (mu - 1.0) / muDoubleScalarPower(a, 3.5) +
                 9.0 * mu * X[2] / muDoubleScalarPower(b_a, 2.5)) - 9.0 * X[2] *
                (mu - 1.0) / muDoubleScalarPower(c_a, 2.5)) - 15.0 * mu *
               d_fXX_tmp / muDoubleScalarPower(d_a, 3.5)) - cb_fXX_tmp * X[2] /
              (b_fXX_tmp * muDoubleScalarPower(e_a, 2.5))) + c_fXX_tmp *
    d_fXX_tmp / (b_fXX_tmp * muDoubleScalarPower(f_a, 3.5));
  fXX[325] = 0.0;
  fXX[425] = 0.0;
  fXX[525] = 0.0;
  st.site = &jd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &jd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &jd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &jd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &jd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &jd_emlrtRSI;
  a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &jd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &jd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &jd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &jd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &jd_emlrtRSI;
  b_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fXX_tmp *= c_a_tmp;
  fXX[625] = fXX_tmp / (y_fXX_tmp * muDoubleScalarPower(a, 2.5)) - bb_fXX_tmp /
    (y_fXX_tmp * muDoubleScalarPower(b_a, 1.5));
  fXX[725] = 0.0;
  fXX[825] = 0.0;
  fXX[925] = 0.0;
  fXX[35] = 0.0;
  fXX[135] = 0.0;
  fXX[235] = 0.0;
  fXX[335] = 0.0;
  fXX[435] = 0.0;
  fXX[535] = 0.0;
  fXX[635] = 0.0;
  fXX[735] = 0.0;
  fXX[835] = 0.0;
  fXX[935] = 0.0;
  fXX[45] = 0.0;
  fXX[145] = 0.0;
  fXX[245] = 0.0;
  fXX[345] = 0.0;
  fXX[445] = 0.0;
  fXX[545] = 0.0;
  fXX[645] = 0.0;
  fXX[745] = 0.0;
  fXX[845] = 0.0;
  fXX[945] = 0.0;
  fXX[55] = 0.0;
  fXX[155] = 0.0;
  fXX[255] = 0.0;
  fXX[355] = 0.0;
  fXX[455] = 0.0;
  fXX[555] = 0.0;
  fXX[655] = 0.0;
  fXX[755] = 0.0;
  fXX[855] = 0.0;
  fXX[955] = 0.0;
  st.site = &kd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &kd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &kd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &kd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &kd_emlrtRSI;
  a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fXX[65] = w_fXX_tmp / (ab_fXX_tmp * muDoubleScalarPower(a, 2.5));
  st.site = &ld_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ld_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ld_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ld_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ld_emlrtRSI;
  a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fXX[165] = i_a / (y_fXX_tmp * muDoubleScalarPower(a, 2.5));
  st.site = &md_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &md_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &md_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &md_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &md_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &md_emlrtRSI;
  a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &md_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &md_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &md_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &md_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &md_emlrtRSI;
  b_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fXX[265] = fXX_tmp / (y_fXX_tmp * muDoubleScalarPower(a, 2.5)) - bb_fXX_tmp /
    (y_fXX_tmp * muDoubleScalarPower(b_a, 1.5));
  fXX[365] = 0.0;
  fXX[465] = 0.0;
  fXX[565] = 0.0;
  st.site = &nd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &nd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &nd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &nd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &nd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &nd_emlrtRSI;
  a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fXX[665] = 2.0 * Tmax * X[9] / e_fXX_tmp + f_fXX_tmp * X[2] / (g_fXX_tmp *
    muDoubleScalarPower(a, 1.5));
  fXX[765] = 0.0;
  fXX[865] = 0.0;
  st.site = &od_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  fXX[965] = x_fXX_tmp;
  fXX[75] = 0.0;
  fXX[175] = 0.0;
  fXX[275] = 0.0;
  fXX[375] = 0.0;
  fXX[475] = 0.0;
  fXX[575] = 0.0;
  fXX[675] = 0.0;
  fXX[775] = 0.0;
  fXX[875] = 0.0;
  fXX[975] = 0.0;
  fXX[85] = 0.0;
  fXX[185] = 0.0;
  fXX[285] = 0.0;
  fXX[385] = 0.0;
  fXX[485] = 0.0;
  fXX[585] = 0.0;
  fXX[685] = 0.0;
  fXX[785] = 0.0;
  fXX[885] = 0.0;
  fXX[985] = 0.0;
  fXX[95] = 0.0;
  fXX[195] = 0.0;
  fXX[295] = 0.0;
  fXX[395] = 0.0;
  fXX[495] = 0.0;
  fXX[595] = 0.0;
  st.site = &pd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  fXX[695] = x_fXX_tmp;
  fXX[795] = 0.0;
  fXX[895] = 0.0;
  fXX[995] = 0.0;
  fXX[6] = 0.0;
  fXX[106] = 0.0;
  fXX[206] = 0.0;
  fXX[306] = 0.0;
  fXX[406] = 0.0;
  fXX[506] = 0.0;
  fXX[606] = 0.0;
  fXX[706] = 0.0;
  fXX[806] = 0.0;
  fXX[906] = 0.0;
  fXX[16] = 0.0;
  fXX[116] = 0.0;
  fXX[216] = 0.0;
  fXX[316] = 0.0;
  fXX[416] = 0.0;
  fXX[516] = 0.0;
  fXX[616] = 0.0;
  fXX[716] = 0.0;
  fXX[816] = 0.0;
  fXX[916] = 0.0;
  fXX[26] = 0.0;
  fXX[126] = 0.0;
  fXX[226] = 0.0;
  fXX[326] = 0.0;
  fXX[426] = 0.0;
  fXX[526] = 0.0;
  fXX[626] = 0.0;
  fXX[726] = 0.0;
  fXX[826] = 0.0;
  fXX[926] = 0.0;
  fXX[36] = 0.0;
  fXX[136] = 0.0;
  fXX[236] = 0.0;
  fXX[336] = 0.0;
  fXX[436] = 0.0;
  fXX[536] = 0.0;
  fXX[636] = 0.0;
  fXX[736] = 0.0;
  fXX[836] = 0.0;
  fXX[936] = 0.0;
  fXX[46] = 0.0;
  fXX[146] = 0.0;
  fXX[246] = 0.0;
  fXX[346] = 0.0;
  fXX[446] = 0.0;
  fXX[546] = 0.0;
  fXX[646] = 0.0;
  fXX[746] = 0.0;
  fXX[846] = 0.0;
  fXX[946] = 0.0;
  fXX[56] = 0.0;
  fXX[156] = 0.0;
  fXX[256] = 0.0;
  fXX[356] = 0.0;
  fXX[456] = 0.0;
  fXX[556] = 0.0;
  fXX[656] = 0.0;
  fXX[756] = 0.0;
  fXX[856] = 0.0;
  fXX[956] = 0.0;
  fXX[66] = 0.0;
  fXX[166] = 0.0;
  fXX[266] = 0.0;
  fXX[366] = 0.0;
  fXX[466] = 0.0;
  fXX[566] = 0.0;
  fXX[666] = 0.0;
  fXX[766] = 0.0;
  fXX[866] = 0.0;
  fXX[966] = 0.0;
  fXX[76] = 0.0;
  fXX[176] = 0.0;
  fXX[276] = 0.0;
  fXX[376] = 0.0;
  fXX[476] = 0.0;
  fXX[576] = 0.0;
  fXX[676] = 0.0;
  st.site = &qd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &qd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &qd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &qd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &qd_emlrtRSI;
  h_a = X[7] * X[7];
  i_a = X[8] * X[8];
  e_fXX_tmp_tmp = X[9] * X[9];
  a_tmp = ((h_a + i_a) + e_fXX_tmp_tmp) + 1.0E-8;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a_tmp < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &qd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &qd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &qd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &qd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  d_st.site = &gb_emlrtRSI;
  e_st.site = &hb_emlrtRSI;
  f_st.site = &ib_emlrtRSI;
  if (a_tmp < 0.0) {
    emlrtErrorWithMessageIdR2018a(&f_st, &b_emlrtRTEI,
      "Coder:toolbox:ElFunDomainError", "Coder:toolbox:ElFunDomainError", 3, 4,
      4, "sqrt");
  }

  fXX[776] = Tmax * h_a / (c * muDoubleScalarPower(a_tmp, 1.5)) - Tmax / (c *
    muDoubleScalarSqrt(a_tmp));
  st.site = &rd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &rd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &rd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &rd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a_tmp < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fXX_tmp = Tmax * X[7];
  b_fXX_tmp = fXX_tmp * X[8];
  fXX[876] = b_fXX_tmp / (c * muDoubleScalarPower(a_tmp, 1.5));
  st.site = &sd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &sd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &sd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &sd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a_tmp < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fXX_tmp *= X[9];
  fXX[976] = fXX_tmp / (c * muDoubleScalarPower(a_tmp, 1.5));
  fXX[86] = 0.0;
  fXX[186] = 0.0;
  fXX[286] = 0.0;
  fXX[386] = 0.0;
  fXX[486] = 0.0;
  fXX[586] = 0.0;
  fXX[686] = 0.0;
  st.site = &td_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &td_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &td_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &td_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a_tmp < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fXX[786] = b_fXX_tmp / (c * muDoubleScalarPower(a_tmp, 1.5));
  st.site = &ud_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ud_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ud_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ud_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ud_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a_tmp < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &ud_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ud_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ud_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ud_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  d_st.site = &gb_emlrtRSI;
  e_st.site = &hb_emlrtRSI;
  f_st.site = &ib_emlrtRSI;
  if (a_tmp < 0.0) {
    emlrtErrorWithMessageIdR2018a(&f_st, &b_emlrtRTEI,
      "Coder:toolbox:ElFunDomainError", "Coder:toolbox:ElFunDomainError", 3, 4,
      4, "sqrt");
  }

  fXX[886] = Tmax * i_a / (c * muDoubleScalarPower(a_tmp, 1.5)) - Tmax / (c *
    muDoubleScalarSqrt(a_tmp));
  st.site = &vd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &vd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &vd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &vd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a_tmp < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  b_fXX_tmp = Tmax * X[8] * X[9];
  fXX[986] = b_fXX_tmp / (c * muDoubleScalarPower(a_tmp, 1.5));
  fXX[96] = 0.0;
  fXX[196] = 0.0;
  fXX[296] = 0.0;
  fXX[396] = 0.0;
  fXX[496] = 0.0;
  fXX[596] = 0.0;
  fXX[696] = 0.0;
  st.site = &wd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &wd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &wd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &wd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a_tmp < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fXX[796] = fXX_tmp / (c * muDoubleScalarPower(a_tmp, 1.5));
  st.site = &xd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &xd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &xd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &xd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a_tmp < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fXX[896] = b_fXX_tmp / (c * muDoubleScalarPower(a_tmp, 1.5));
  st.site = &yd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &yd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &yd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &yd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &yd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a_tmp < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &yd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &yd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &yd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &yd_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  d_st.site = &gb_emlrtRSI;
  e_st.site = &hb_emlrtRSI;
  f_st.site = &ib_emlrtRSI;
  if (a_tmp < 0.0) {
    emlrtErrorWithMessageIdR2018a(&f_st, &b_emlrtRTEI,
      "Coder:toolbox:ElFunDomainError", "Coder:toolbox:ElFunDomainError", 3, 4,
      4, "sqrt");
  }

  fXX[996] = Tmax * e_fXX_tmp_tmp / (c * muDoubleScalarPower(a_tmp, 1.5)) - Tmax
    / (c * muDoubleScalarSqrt(a_tmp));
  fXX[7] = 0.0;
  fXX[107] = 0.0;
  fXX[207] = 0.0;
  fXX[307] = 0.0;
  fXX[407] = 0.0;
  fXX[507] = 0.0;
  fXX[607] = 0.0;
  fXX[707] = 0.0;
  fXX[807] = 0.0;
  fXX[907] = 0.0;
  fXX[17] = 0.0;
  fXX[117] = 0.0;
  fXX[217] = 0.0;
  fXX[317] = 0.0;
  fXX[417] = 0.0;
  fXX[517] = 0.0;
  fXX[617] = 0.0;
  fXX[717] = 0.0;
  fXX[817] = 0.0;
  fXX[917] = 0.0;
  fXX[27] = 0.0;
  fXX[127] = 0.0;
  fXX[227] = 0.0;
  fXX[327] = 0.0;
  fXX[427] = 0.0;
  fXX[527] = 0.0;
  fXX[627] = 0.0;
  fXX[727] = 0.0;
  fXX[827] = 0.0;
  fXX[927] = 0.0;
  fXX[37] = 0.0;
  fXX[137] = 0.0;
  fXX[237] = 0.0;
  fXX[337] = 0.0;
  fXX[437] = 0.0;
  fXX[537] = 0.0;
  fXX[637] = 0.0;
  fXX[737] = 0.0;
  fXX[837] = 0.0;
  fXX[937] = 0.0;
  fXX[47] = 0.0;
  fXX[147] = 0.0;
  fXX[247] = 0.0;
  fXX[347] = 0.0;
  fXX[447] = 0.0;
  fXX[547] = 0.0;
  fXX[647] = 0.0;
  fXX[747] = 0.0;
  fXX[847] = 0.0;
  fXX[947] = 0.0;
  fXX[57] = 0.0;
  fXX[157] = 0.0;
  fXX[257] = 0.0;
  fXX[357] = 0.0;
  fXX[457] = 0.0;
  fXX[557] = 0.0;
  fXX[657] = 0.0;
  fXX[757] = 0.0;
  fXX[857] = 0.0;
  fXX[957] = 0.0;
  fXX[67] = 0.0;
  fXX[167] = 0.0;
  fXX[267] = 0.0;
  fXX[367] = 0.0;
  fXX[467] = 0.0;
  fXX[567] = 0.0;
  fXX[667] = 0.0;
  fXX[767] = 0.0;
  fXX[867] = 0.0;
  fXX[967] = 0.0;
  fXX[77] = 0.0;
  fXX[177] = 0.0;
  fXX[277] = 0.0;
  fXX[377] = 0.0;
  fXX[477] = 0.0;
  fXX[577] = 0.0;
  fXX[677] = 0.0;
  fXX[777] = 0.0;
  fXX[877] = 0.0;
  fXX[977] = 0.0;
  fXX[87] = 0.0;
  fXX[187] = 0.0;
  fXX[287] = 0.0;
  fXX[387] = 0.0;
  fXX[487] = 0.0;
  fXX[587] = 0.0;
  fXX[687] = 0.0;
  fXX[787] = 0.0;
  fXX[887] = 0.0;
  fXX[987] = 0.0;
  fXX[97] = 0.0;
  fXX[197] = 0.0;
  fXX[297] = 0.0;
  fXX[397] = 0.0;
  fXX[497] = 0.0;
  fXX[597] = 0.0;
  fXX[697] = 0.0;
  fXX[797] = 0.0;
  fXX[897] = 0.0;
  fXX[997] = 0.0;
  fXX[8] = 0.0;
  fXX[108] = 0.0;
  fXX[208] = 0.0;
  fXX[308] = 0.0;
  fXX[408] = 0.0;
  fXX[508] = 0.0;
  fXX[608] = 0.0;
  fXX[708] = 0.0;
  fXX[808] = 0.0;
  fXX[908] = 0.0;
  fXX[18] = 0.0;
  fXX[118] = 0.0;
  fXX[218] = 0.0;
  fXX[318] = 0.0;
  fXX[418] = 0.0;
  fXX[518] = 0.0;
  fXX[618] = 0.0;
  fXX[718] = 0.0;
  fXX[818] = 0.0;
  fXX[918] = 0.0;
  fXX[28] = 0.0;
  fXX[128] = 0.0;
  fXX[228] = 0.0;
  fXX[328] = 0.0;
  fXX[428] = 0.0;
  fXX[528] = 0.0;
  fXX[628] = 0.0;
  fXX[728] = 0.0;
  fXX[828] = 0.0;
  fXX[928] = 0.0;
  fXX[38] = 0.0;
  fXX[138] = 0.0;
  fXX[238] = 0.0;
  fXX[338] = 0.0;
  fXX[438] = 0.0;
  fXX[538] = 0.0;
  fXX[638] = 0.0;
  fXX[738] = 0.0;
  fXX[838] = 0.0;
  fXX[938] = 0.0;
  fXX[48] = 0.0;
  fXX[148] = 0.0;
  fXX[248] = 0.0;
  fXX[348] = 0.0;
  fXX[448] = 0.0;
  fXX[548] = 0.0;
  fXX[648] = 0.0;
  fXX[748] = 0.0;
  fXX[848] = 0.0;
  fXX[948] = 0.0;
  fXX[58] = 0.0;
  fXX[158] = 0.0;
  fXX[258] = 0.0;
  fXX[358] = 0.0;
  fXX[458] = 0.0;
  fXX[558] = 0.0;
  fXX[658] = 0.0;
  fXX[758] = 0.0;
  fXX[858] = 0.0;
  fXX[958] = 0.0;
  fXX[68] = 0.0;
  fXX[168] = 0.0;
  fXX[268] = 0.0;
  fXX[368] = 0.0;
  fXX[468] = 0.0;
  fXX[568] = 0.0;
  fXX[668] = 0.0;
  fXX[768] = 0.0;
  fXX[868] = 0.0;
  fXX[968] = 0.0;
  fXX[78] = 0.0;
  fXX[178] = 0.0;
  fXX[278] = 0.0;
  fXX[378] = 0.0;
  fXX[478] = 0.0;
  fXX[578] = 0.0;
  fXX[678] = 0.0;
  fXX[778] = 0.0;
  fXX[878] = 0.0;
  fXX[978] = 0.0;
  fXX[88] = 0.0;
  fXX[188] = 0.0;
  fXX[288] = 0.0;
  fXX[388] = 0.0;
  fXX[488] = 0.0;
  fXX[588] = 0.0;
  fXX[688] = 0.0;
  fXX[788] = 0.0;
  fXX[888] = 0.0;
  fXX[988] = 0.0;
  fXX[98] = 0.0;
  fXX[198] = 0.0;
  fXX[298] = 0.0;
  fXX[398] = 0.0;
  fXX[498] = 0.0;
  fXX[598] = 0.0;
  fXX[698] = 0.0;
  fXX[798] = 0.0;
  fXX[898] = 0.0;
  fXX[998] = 0.0;
  fXX[9] = 0.0;
  fXX[109] = 0.0;
  fXX[209] = 0.0;
  fXX[309] = 0.0;
  fXX[409] = 0.0;
  fXX[509] = 0.0;
  fXX[609] = 0.0;
  fXX[709] = 0.0;
  fXX[809] = 0.0;
  fXX[909] = 0.0;
  fXX[19] = 0.0;
  fXX[119] = 0.0;
  fXX[219] = 0.0;
  fXX[319] = 0.0;
  fXX[419] = 0.0;
  fXX[519] = 0.0;
  fXX[619] = 0.0;
  fXX[719] = 0.0;
  fXX[819] = 0.0;
  fXX[919] = 0.0;
  fXX[29] = 0.0;
  fXX[129] = 0.0;
  fXX[229] = 0.0;
  fXX[329] = 0.0;
  fXX[429] = 0.0;
  fXX[529] = 0.0;
  fXX[629] = 0.0;
  fXX[729] = 0.0;
  fXX[829] = 0.0;
  fXX[929] = 0.0;
  fXX[39] = 0.0;
  fXX[139] = 0.0;
  fXX[239] = 0.0;
  fXX[339] = 0.0;
  fXX[439] = 0.0;
  fXX[539] = 0.0;
  fXX[639] = 0.0;
  fXX[739] = 0.0;
  fXX[839] = 0.0;
  fXX[939] = 0.0;
  fXX[49] = 0.0;
  fXX[149] = 0.0;
  fXX[249] = 0.0;
  fXX[349] = 0.0;
  fXX[449] = 0.0;
  fXX[549] = 0.0;
  fXX[649] = 0.0;
  fXX[749] = 0.0;
  fXX[849] = 0.0;
  fXX[949] = 0.0;
  fXX[59] = 0.0;
  fXX[159] = 0.0;
  fXX[259] = 0.0;
  fXX[359] = 0.0;
  fXX[459] = 0.0;
  fXX[559] = 0.0;
  fXX[659] = 0.0;
  fXX[759] = 0.0;
  fXX[859] = 0.0;
  fXX[959] = 0.0;
  fXX[69] = 0.0;
  fXX[169] = 0.0;
  fXX[269] = 0.0;
  fXX[369] = 0.0;
  fXX[469] = 0.0;
  fXX[569] = 0.0;
  fXX[669] = 0.0;
  fXX[769] = 0.0;
  fXX[869] = 0.0;
  fXX[969] = 0.0;
  fXX[79] = 0.0;
  fXX[179] = 0.0;
  fXX[279] = 0.0;
  fXX[379] = 0.0;
  fXX[479] = 0.0;
  fXX[579] = 0.0;
  fXX[679] = 0.0;
  fXX[779] = 0.0;
  fXX[879] = 0.0;
  fXX[979] = 0.0;
  fXX[89] = 0.0;
  fXX[189] = 0.0;
  fXX[289] = 0.0;
  fXX[389] = 0.0;
  fXX[489] = 0.0;
  fXX[589] = 0.0;
  fXX[689] = 0.0;
  fXX[789] = 0.0;
  fXX[889] = 0.0;
  fXX[989] = 0.0;
  fXX[99] = 0.0;
  fXX[199] = 0.0;
  fXX[299] = 0.0;
  fXX[399] = 0.0;
  fXX[499] = 0.0;
  fXX[599] = 0.0;
  fXX[699] = 0.0;
  fXX[799] = 0.0;
  fXX[899] = 0.0;
  fXX[999] = 0.0;
}

/* End of code generation (fXX_SRP.c) */
