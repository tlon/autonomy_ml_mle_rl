/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * CR3BP_SRP_cart_control_STMSTT.c
 *
 * Code generation for function 'CR3BP_SRP_cart_control_STMSTT'
 *
 */

/* Include files */
#include <string.h>
#include "mwmathutil.h"
#include "rt_nonfinite.h"
#include "CR3BP_SRP_cart_control_STMSTT.h"
#include "fXX_SRP.h"
#include "fX_SRP.h"
#include "sqrt.h"
#include "CR3BP_SRP_cart_control_STMSTT_data.h"
#include "blas.h"

/* Variable Definitions */
static emlrtRSInfo emlrtRSI = { 32,    /* lineNo */
  "CR3BP_SRP_cart_control_STMSTT",     /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\CR3BP_SRP_cart_control_STMSTT.m"/* pathName */
};

static emlrtRSInfo b_emlrtRSI = { 33,  /* lineNo */
  "CR3BP_SRP_cart_control_STMSTT",     /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\CR3BP_SRP_cart_control_STMSTT.m"/* pathName */
};

static emlrtRSInfo c_emlrtRSI = { 36,  /* lineNo */
  "CR3BP_SRP_cart_control_STMSTT",     /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\CR3BP_SRP_cart_control_STMSTT.m"/* pathName */
};

static emlrtRSInfo d_emlrtRSI = { 37,  /* lineNo */
  "CR3BP_SRP_cart_control_STMSTT",     /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\CR3BP_SRP_cart_control_STMSTT.m"/* pathName */
};

static emlrtRSInfo e_emlrtRSI = { 38,  /* lineNo */
  "CR3BP_SRP_cart_control_STMSTT",     /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\CR3BP_SRP_cart_control_STMSTT.m"/* pathName */
};

static emlrtRSInfo f_emlrtRSI = { 46,  /* lineNo */
  "CR3BP_SRP_cart_control_STMSTT",     /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\CR3BP_SRP_cart_control_STMSTT.m"/* pathName */
};

static emlrtRSInfo g_emlrtRSI = { 49,  /* lineNo */
  "CR3BP_SRP_cart_control_STMSTT",     /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\CR3BP_SRP_cart_control_STMSTT.m"/* pathName */
};

static emlrtRSInfo h_emlrtRSI = { 50,  /* lineNo */
  "CR3BP_SRP_cart_control_STMSTT",     /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\CR3BP_SRP_cart_control_STMSTT.m"/* pathName */
};

static emlrtRSInfo i_emlrtRSI = { 51,  /* lineNo */
  "CR3BP_SRP_cart_control_STMSTT",     /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\CR3BP_SRP_cart_control_STMSTT.m"/* pathName */
};

static emlrtRSInfo j_emlrtRSI = { 59,  /* lineNo */
  "CR3BP_SRP_cart_control_STMSTT",     /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\CR3BP_SRP_cart_control_STMSTT.m"/* pathName */
};

static emlrtRSInfo k_emlrtRSI = { 60,  /* lineNo */
  "CR3BP_SRP_cart_control_STMSTT",     /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\CR3BP_SRP_cart_control_STMSTT.m"/* pathName */
};

static emlrtRSInfo l_emlrtRSI = { 64,  /* lineNo */
  "CR3BP_SRP_cart_control_STMSTT",     /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\CR3BP_SRP_cart_control_STMSTT.m"/* pathName */
};

static emlrtRSInfo m_emlrtRSI = { 69,  /* lineNo */
  "CR3BP_SRP_cart_control_STMSTT",     /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\CR3BP_SRP_cart_control_STMSTT.m"/* pathName */
};

static emlrtRSInfo n_emlrtRSI = { 72,  /* lineNo */
  "CR3BP_SRP_cart_control_STMSTT",     /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\CR3BP_SRP_cart_control_STMSTT.m"/* pathName */
};

static emlrtRSInfo jb_emlrtRSI = { 54, /* lineNo */
  "eml_mtimes_helper",                 /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\ops\\eml_mtimes_helper.m"/* pathName */
};

/* Function Definitions */
void CR3BP_SRP_cart_control_STMSTT(const emlrtStack *sp, real_T t, const real_T
  X[1110], real_T mu, real_T c, real_T Tmax, real_T C_R, real_T Phi, real_T A_sc,
  real_T c_light, real_T X_dot[1110])
{
  real_T STM[100];
  real_T a_tmp;
  real_T alpha1;
  real_T beta1;
  real_T r1;
  real_T b_a_tmp;
  real_T r2;
  real_T d0;
  real_T a;
  real_T b_a;
  real_T c_a;
  real_T fXmat[100];
  ptrdiff_t m_t;
  ptrdiff_t n_t;
  ptrdiff_t k_t;
  ptrdiff_t lda_t;
  ptrdiff_t ldb_t;
  ptrdiff_t ldc_t;
  char_T TRANSA;
  char_T TRANSB;
  real_T STMdot[100];
  real_T fXXten[1000];
  real_T b_alpha1;
  real_T b_beta1;
  ptrdiff_t b_m_t;
  ptrdiff_t b_n_t;
  ptrdiff_t b_k_t;
  ptrdiff_t b_lda_t;
  ptrdiff_t b_ldb_t;
  ptrdiff_t b_ldc_t;
  char_T b_TRANSA;
  char_T b_TRANSB;
  int32_T i;
  int32_T i0;
  int32_T i1;
  real_T b_X[100];
  real_T b[100];
  real_T d_a[100];
  real_T c_X[1000];
  int32_T fXXten_tmp;
  real_T temp[1000];
  int32_T b_fXXten_tmp;
  emlrtStack st;
  emlrtStack b_st;
  emlrtStack c_st;
  (void)t;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;

  /*     %% Unpack */
  /*  Reshape to column vector */
  /*  Positions */
  /*  Velocities */
  /*  Mass */
  /*  Control */
  memcpy(&STM[0], &X[10], 100U * sizeof(real_T));

  /*     %% Dynamics */
  /*  Distances to primaries */
  st.site = &emlrtRSI;
  a_tmp = X[0] + mu;
  b_st.site = &o_emlrtRSI;
  st.site = &emlrtRSI;
  b_st.site = &o_emlrtRSI;
  st.site = &emlrtRSI;
  b_st.site = &o_emlrtRSI;
  alpha1 = X[1] * X[1];
  beta1 = X[2] * X[2];
  r1 = (a_tmp * a_tmp + alpha1) + beta1;
  st.site = &emlrtRSI;
  b_sqrt(&st, &r1);
  st.site = &b_emlrtRSI;
  b_a_tmp = (X[0] - 1.0) + mu;
  b_st.site = &o_emlrtRSI;
  st.site = &b_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  st.site = &b_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  r2 = (b_a_tmp * b_a_tmp + alpha1) + beta1;
  st.site = &b_emlrtRSI;
  b_sqrt(&st, &r2);

  /*  Accelerations  */
  st.site = &c_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  st.site = &c_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  st.site = &d_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  st.site = &d_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  st.site = &e_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  st.site = &e_emlrtRSI;
  b_st.site = &o_emlrtRSI;

  /*  Control */
  /*  Mass Flow Rate */
  st.site = &f_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  st.site = &f_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  st.site = &f_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  d0 = ((X[7] * X[7] + X[8] * X[8]) + X[9] * X[9]) + 1.0E-8;
  st.site = &f_emlrtRSI;
  b_sqrt(&st, &d0);

  /*  SRP */
  st.site = &g_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  st.site = &g_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  st.site = &g_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  st.site = &g_emlrtRSI;
  a = (a_tmp * a_tmp + alpha1) + beta1;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &h_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  st.site = &h_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  st.site = &h_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  st.site = &h_emlrtRSI;
  b_a = (a_tmp * a_tmp + alpha1) + beta1;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &i_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  st.site = &i_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  st.site = &i_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  st.site = &i_emlrtRSI;
  c_a = (a_tmp * a_tmp + alpha1) + beta1;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  /*  Total Acceleration */
  /*     %% STM Dynamics */
  st.site = &j_emlrtRSI;
  fX_SRP(&st, X, c, mu, Tmax, C_R, Phi, A_sc, c_light, fXmat);
  st.site = &k_emlrtRSI;
  b_st.site = &jb_emlrtRSI;
  alpha1 = 1.0;
  beta1 = 0.0;
  m_t = (ptrdiff_t)10;
  n_t = (ptrdiff_t)10;
  k_t = (ptrdiff_t)10;
  lda_t = (ptrdiff_t)10;
  ldb_t = (ptrdiff_t)10;
  ldc_t = (ptrdiff_t)10;
  TRANSA = 'N';
  TRANSB = 'N';
  dgemm(&TRANSA, &TRANSB, &m_t, &n_t, &k_t, &alpha1, &fXmat[0], &lda_t, &STM[0],
        &ldb_t, &beta1, &STMdot[0], &ldc_t);

  /*     %% STT Dynamics */
  st.site = &l_emlrtRSI;
  fXX_SRP(&st, X, c, mu, Tmax, C_R, Phi, A_sc, c_light, fXXten);
  alpha1 = 1.0;
  beta1 = 0.0;
  m_t = (ptrdiff_t)10;
  n_t = (ptrdiff_t)10;
  k_t = (ptrdiff_t)10;
  lda_t = (ptrdiff_t)10;
  ldb_t = (ptrdiff_t)10;
  ldc_t = (ptrdiff_t)10;
  TRANSA = 'N';
  TRANSB = 'N';
  b_alpha1 = 1.0;
  b_beta1 = 0.0;
  b_m_t = (ptrdiff_t)10;
  b_n_t = (ptrdiff_t)10;
  b_k_t = (ptrdiff_t)10;
  b_lda_t = (ptrdiff_t)10;
  b_ldb_t = (ptrdiff_t)10;
  b_ldc_t = (ptrdiff_t)10;
  b_TRANSA = 'N';
  b_TRANSB = 'N';
  for (i = 0; i < 10; i++) {
    for (i0 = 0; i0 < 10; i0++) {
      for (i1 = 0; i1 < 10; i1++) {
        b[i1 + 10 * i0] = fXXten[(i + 10 * i1) + 100 * i0];
      }
    }

    st.site = &m_emlrtRSI;
    memcpy(&b_X[0], &X[10], 100U * sizeof(real_T));
    for (i0 = 0; i0 < 10; i0++) {
      for (i1 = 0; i1 < 10; i1++) {
        d_a[i1 + 10 * i0] = b_X[i0 + 10 * i1];
      }
    }

    b_st.site = &jb_emlrtRSI;
    dgemm(&TRANSA, &TRANSB, &m_t, &n_t, &k_t, &alpha1, &d_a[0], &lda_t, &b[0],
          &ldb_t, &beta1, &b_X[0], &ldc_t);
    st.site = &m_emlrtRSI;
    b_st.site = &jb_emlrtRSI;
    dgemm(&b_TRANSA, &b_TRANSB, &b_m_t, &b_n_t, &b_k_t, &b_alpha1, &b_X[0],
          &b_lda_t, &STM[0], &b_ldb_t, &b_beta1, &d_a[0], &b_ldc_t);
    for (i0 = 0; i0 < 10; i0++) {
      for (i1 = 0; i1 < 10; i1++) {
        temp[(i + 10 * i1) + 100 * i0] = d_a[i1 + 10 * i0];
      }
    }

    if (*emlrtBreakCheckR2012bFlagVar != 0) {
      emlrtBreakCheckR2012b(sp);
    }
  }

  alpha1 = 1.0;
  beta1 = 0.0;
  m_t = (ptrdiff_t)10;
  n_t = (ptrdiff_t)10;
  k_t = (ptrdiff_t)10;
  lda_t = (ptrdiff_t)10;
  ldb_t = (ptrdiff_t)10;
  ldc_t = (ptrdiff_t)10;
  TRANSA = 'N';
  TRANSB = 'N';
  for (i = 0; i < 10; i++) {
    st.site = &n_emlrtRSI;
    memcpy(&c_X[0], &X[110], 1000U * sizeof(real_T));
    for (i0 = 0; i0 < 10; i0++) {
      memcpy(&b[i0 * 10], &c_X[i * 100 + i0 * 10], 10U * sizeof(real_T));
    }

    b_st.site = &jb_emlrtRSI;
    dgemm(&TRANSA, &TRANSB, &m_t, &n_t, &k_t, &alpha1, &fXmat[0], &lda_t, &b[0],
          &ldb_t, &beta1, &d_a[0], &ldc_t);
    for (i0 = 0; i0 < 10; i0++) {
      for (i1 = 0; i1 < 10; i1++) {
        fXXten_tmp = i1 + 10 * i0;
        b_fXXten_tmp = fXXten_tmp + 100 * i;
        fXXten[b_fXXten_tmp] = d_a[fXXten_tmp] + temp[b_fXXten_tmp];
      }
    }

    if (*emlrtBreakCheckR2012bFlagVar != 0) {
      emlrtBreakCheckR2012b(sp);
    }
  }

  b_alpha1 = muDoubleScalarPower(r1, 3.0);
  alpha1 = muDoubleScalarPower(r2, 3.0);
  beta1 = C_R * Phi;
  X_dot[6] = -Tmax * d0 / c;
  X_dot[0] = X[3];
  X_dot[3] = ((((2.0 * X[4] + X[0]) - (1.0 - mu) * (a_tmp / b_alpha1)) - mu *
               b_a_tmp / alpha1) + beta1 / (c_light * muDoubleScalarPower(a, 1.5))
              * A_sc * a_tmp) + Tmax * (X[7] / X[6]);
  X_dot[7] = 0.0;
  X_dot[1] = X[4];
  X_dot[4] = ((((-2.0 * X[3] + X[1]) - (1.0 - mu) * (X[1] / b_alpha1)) - mu * X
               [1] / alpha1) + beta1 / (c_light * muDoubleScalarPower(b_a, 1.5))
              * A_sc * X[1]) + Tmax * (X[8] / X[6]);
  X_dot[8] = 0.0;
  X_dot[2] = X[5];
  X_dot[5] = ((-(1.0 - mu) * X[2] / b_alpha1 - mu * X[2] / alpha1) + beta1 /
              (c_light * muDoubleScalarPower(c_a, 1.5)) * A_sc * X[2]) + Tmax *
    (X[9] / X[6]);
  X_dot[9] = 0.0;
  memcpy(&X_dot[10], &STMdot[0], 100U * sizeof(real_T));
  memcpy(&X_dot[110], &fXXten[0], 1000U * sizeof(real_T));
}

/* End of code generation (CR3BP_SRP_cart_control_STMSTT.c) */
