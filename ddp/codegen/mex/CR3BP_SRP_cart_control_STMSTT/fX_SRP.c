/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * fX_SRP.c
 *
 * Code generation for function 'fX_SRP'
 *
 */

/* Include files */
#include "mwmathutil.h"
#include "rt_nonfinite.h"
#include "CR3BP_SRP_cart_control_STMSTT.h"
#include "fX_SRP.h"
#include "CR3BP_SRP_cart_control_STMSTT_data.h"

/* Variable Definitions */
static emlrtRSInfo q_emlrtRSI = { 47,  /* lineNo */
  "fX_SRP",                            /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fX_SRP.m"                   /* pathName */
};

static emlrtRSInfo r_emlrtRSI = { 48,  /* lineNo */
  "fX_SRP",                            /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fX_SRP.m"                   /* pathName */
};

static emlrtRSInfo s_emlrtRSI = { 49,  /* lineNo */
  "fX_SRP",                            /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fX_SRP.m"                   /* pathName */
};

static emlrtRSInfo t_emlrtRSI = { 53,  /* lineNo */
  "fX_SRP",                            /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fX_SRP.m"                   /* pathName */
};

static emlrtRSInfo u_emlrtRSI = { 58,  /* lineNo */
  "fX_SRP",                            /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fX_SRP.m"                   /* pathName */
};

static emlrtRSInfo v_emlrtRSI = { 59,  /* lineNo */
  "fX_SRP",                            /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fX_SRP.m"                   /* pathName */
};

static emlrtRSInfo w_emlrtRSI = { 60,  /* lineNo */
  "fX_SRP",                            /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fX_SRP.m"                   /* pathName */
};

static emlrtRSInfo x_emlrtRSI = { 64,  /* lineNo */
  "fX_SRP",                            /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fX_SRP.m"                   /* pathName */
};

static emlrtRSInfo y_emlrtRSI = { 69,  /* lineNo */
  "fX_SRP",                            /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fX_SRP.m"                   /* pathName */
};

static emlrtRSInfo ab_emlrtRSI = { 70, /* lineNo */
  "fX_SRP",                            /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fX_SRP.m"                   /* pathName */
};

static emlrtRSInfo bb_emlrtRSI = { 71, /* lineNo */
  "fX_SRP",                            /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fX_SRP.m"                   /* pathName */
};

static emlrtRSInfo cb_emlrtRSI = { 75, /* lineNo */
  "fX_SRP",                            /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fX_SRP.m"                   /* pathName */
};

static emlrtRSInfo db_emlrtRSI = { 87, /* lineNo */
  "fX_SRP",                            /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fX_SRP.m"                   /* pathName */
};

static emlrtRSInfo eb_emlrtRSI = { 88, /* lineNo */
  "fX_SRP",                            /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fX_SRP.m"                   /* pathName */
};

static emlrtRSInfo fb_emlrtRSI = { 89, /* lineNo */
  "fX_SRP",                            /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\ddp\\fX_SRP.m"                   /* pathName */
};

/* Function Definitions */
void fX_SRP(const emlrtStack *sp, const real_T X[1110], real_T c, real_T mu,
            real_T Tmax, real_T C_R, real_T Phi, real_T A_sc, real_T c_light,
            real_T fX[100])
{
  real_T a_tmp;
  real_T b_a_tmp;
  real_T c_a_tmp;
  real_T a;
  real_T b_a;
  real_T c_a;
  real_T d_a;
  real_T e_a;
  real_T f_a;
  real_T fX_tmp;
  real_T b_fX_tmp;
  real_T c_fX_tmp;
  real_T d_fX_tmp;
  real_T e_fX_tmp;
  real_T f_fX_tmp;
  real_T g_fX_tmp;
  real_T h_fX_tmp;
  real_T i_fX_tmp;
  real_T j_fX_tmp;
  real_T k_fX_tmp;
  real_T l_fX_tmp;
  real_T m_fX_tmp;
  real_T n_fX_tmp;
  emlrtStack st;
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  emlrtStack e_st;
  emlrtStack f_st;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  e_st.prev = &d_st;
  e_st.tls = d_st.tls;
  f_st.prev = &e_st;
  f_st.tls = e_st.tls;
  fX[0] = 0.0;
  fX[10] = 0.0;
  fX[20] = 0.0;
  fX[30] = 1.0;
  fX[40] = 0.0;
  fX[50] = 0.0;
  fX[60] = 0.0;
  fX[70] = 0.0;
  fX[80] = 0.0;
  fX[90] = 0.0;
  fX[1] = 0.0;
  fX[11] = 0.0;
  fX[21] = 0.0;
  fX[31] = 0.0;
  fX[41] = 1.0;
  fX[51] = 0.0;
  fX[61] = 0.0;
  fX[71] = 0.0;
  fX[81] = 0.0;
  fX[91] = 0.0;
  fX[2] = 0.0;
  fX[12] = 0.0;
  fX[22] = 0.0;
  fX[32] = 0.0;
  fX[42] = 0.0;
  fX[52] = 1.0;
  fX[62] = 0.0;
  fX[72] = 0.0;
  fX[82] = 0.0;
  fX[92] = 0.0;
  st.site = &q_emlrtRSI;
  a_tmp = mu + X[0];
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &q_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &q_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &q_emlrtRSI;
  b_a_tmp = X[1] * X[1];
  c_a_tmp = X[2] * X[2];
  a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &q_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &q_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &q_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &q_emlrtRSI;
  b_a = ((a_tmp - 1.0) * (a_tmp - 1.0) + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &q_emlrtRSI;
  c_a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &q_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &q_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &q_emlrtRSI;
  c_a = (c_a * c_a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &q_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &q_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &q_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &q_emlrtRSI;
  d_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (d_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &q_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &q_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &q_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &q_emlrtRSI;
  e_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (e_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &q_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &q_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &q_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &q_emlrtRSI;
  f_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (f_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fX_tmp = 2.0 * mu + 2.0 * X[0];
  b_fX_tmp = 3.0 * A_sc * C_R * Phi;
  c_fX_tmp = c_light * X[6];
  d_fX_tmp = A_sc * C_R * Phi;
  e_fX_tmp = 2.0 * c_light * X[6];
  fX[3] = ((((((mu - 1.0) / muDoubleScalarPower(a, 1.5) - mu /
               muDoubleScalarPower(b_a, 1.5)) + 3.0 * mu * (fX_tmp - 2.0) * ((mu
    + X[0]) - 1.0) / (2.0 * muDoubleScalarPower(c_a, 2.5))) - 3.0 * fX_tmp *
             a_tmp * (mu - 1.0) / (2.0 * muDoubleScalarPower(d_a, 2.5))) +
            d_fX_tmp / (c_fX_tmp * muDoubleScalarPower(e_a, 1.5))) - b_fX_tmp *
           fX_tmp * a_tmp / (e_fX_tmp * muDoubleScalarPower(f_a, 2.5))) + 1.0;
  st.site = &r_emlrtRSI;
  a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &r_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &r_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &r_emlrtRSI;
  a = (a * a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &r_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &r_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &r_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &r_emlrtRSI;
  b_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &r_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &r_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &r_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &r_emlrtRSI;
  c_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  f_fX_tmp = 3.0 * mu * X[1];
  g_fX_tmp = b_fX_tmp * X[1];
  fX[13] = (f_fX_tmp * ((mu + X[0]) - 1.0) / muDoubleScalarPower(a, 2.5) - 3.0 *
            X[1] * a_tmp * (mu - 1.0) / muDoubleScalarPower(b_a, 2.5)) -
    g_fX_tmp * a_tmp / (c_fX_tmp * muDoubleScalarPower(c_a, 2.5));
  st.site = &s_emlrtRSI;
  a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &s_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &s_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &s_emlrtRSI;
  a = (a * a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &s_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &s_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &s_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &s_emlrtRSI;
  b_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &s_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &s_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &s_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &s_emlrtRSI;
  c_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  h_fX_tmp = 3.0 * mu * X[2];
  i_fX_tmp = b_fX_tmp * X[2];
  fX[23] = (h_fX_tmp * ((mu + X[0]) - 1.0) / muDoubleScalarPower(a, 2.5) - 3.0 *
            X[2] * a_tmp * (mu - 1.0) / muDoubleScalarPower(b_a, 2.5)) -
    i_fX_tmp * a_tmp / (c_fX_tmp * muDoubleScalarPower(c_a, 2.5));
  fX[33] = 0.0;
  fX[43] = 2.0;
  fX[53] = 0.0;
  st.site = &t_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &t_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &t_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &t_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &t_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &t_emlrtRSI;
  a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  j_fX_tmp = X[6] * X[6];
  k_fX_tmp = c_light * j_fX_tmp;
  l_fX_tmp = -(Tmax * X[7]);
  fX[63] = l_fX_tmp / j_fX_tmp - d_fX_tmp * a_tmp / (k_fX_tmp *
    muDoubleScalarPower(a, 1.5));
  m_fX_tmp = Tmax / X[6];
  fX[73] = m_fX_tmp;
  fX[83] = 0.0;
  fX[93] = 0.0;
  st.site = &u_emlrtRSI;
  a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &u_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &u_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &u_emlrtRSI;
  a = (a * a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &u_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &u_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &u_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &u_emlrtRSI;
  b_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &u_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &u_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &u_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &u_emlrtRSI;
  c_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fX[4] = (f_fX_tmp * ((2.0 * mu + 2.0 * X[0]) - 2.0) / (2.0 *
            muDoubleScalarPower(a, 2.5)) - 3.0 * X[1] * fX_tmp * (mu - 1.0) /
           (2.0 * muDoubleScalarPower(b_a, 2.5))) - g_fX_tmp * fX_tmp /
    (e_fX_tmp * muDoubleScalarPower(c_a, 2.5));
  st.site = &v_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &v_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &v_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &v_emlrtRSI;
  a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &v_emlrtRSI;
  b_a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &v_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &v_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &v_emlrtRSI;
  b_a = (b_a * b_a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &v_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &v_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &v_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &v_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &v_emlrtRSI;
  c_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &v_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &v_emlrtRSI;
  d_a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &v_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &v_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &v_emlrtRSI;
  d_a = (d_a * d_a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (d_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &v_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &v_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &v_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &v_emlrtRSI;
  e_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (e_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &v_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &v_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &v_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &v_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &v_emlrtRSI;
  f_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (f_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fX[14] = ((((((mu - 1.0) / muDoubleScalarPower(a, 1.5) - mu /
                muDoubleScalarPower(b_a, 1.5)) - 3.0 * b_a_tmp * (mu - 1.0) /
               muDoubleScalarPower(c_a, 2.5)) + 3.0 * mu * b_a_tmp /
              muDoubleScalarPower(d_a, 2.5)) + d_fX_tmp / (c_fX_tmp *
              muDoubleScalarPower(e_a, 1.5))) - b_fX_tmp * b_a_tmp / (c_fX_tmp *
             muDoubleScalarPower(f_a, 2.5))) + 1.0;
  st.site = &w_emlrtRSI;
  a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &w_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &w_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &w_emlrtRSI;
  a = (a * a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &w_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &w_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &w_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &w_emlrtRSI;
  b_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &w_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &w_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &w_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &w_emlrtRSI;
  c_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  f_fX_tmp *= X[2];
  d_a = 3.0 * X[1] * X[2] * (mu - 1.0);
  g_fX_tmp *= X[2];
  fX[24] = (f_fX_tmp / muDoubleScalarPower(a, 2.5) - d_a / muDoubleScalarPower
            (b_a, 2.5)) - g_fX_tmp / (c_fX_tmp * muDoubleScalarPower(c_a, 2.5));
  fX[34] = -2.0;
  fX[44] = 0.0;
  fX[54] = 0.0;
  st.site = &x_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &x_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &x_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &x_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &x_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &x_emlrtRSI;
  a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  n_fX_tmp = -(Tmax * X[8]);
  fX[64] = n_fX_tmp / j_fX_tmp - d_fX_tmp * X[1] / (k_fX_tmp *
    muDoubleScalarPower(a, 1.5));
  fX[74] = 0.0;
  fX[84] = m_fX_tmp;
  fX[94] = 0.0;
  st.site = &y_emlrtRSI;
  a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &y_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &y_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &y_emlrtRSI;
  a = (a * a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &y_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &y_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &y_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &y_emlrtRSI;
  b_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &y_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &y_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &y_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &y_emlrtRSI;
  c_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fX[5] = (h_fX_tmp * ((2.0 * mu + 2.0 * X[0]) - 2.0) / (2.0 *
            muDoubleScalarPower(a, 2.5)) - 3.0 * X[2] * fX_tmp * (mu - 1.0) /
           (2.0 * muDoubleScalarPower(b_a, 2.5))) - i_fX_tmp * fX_tmp /
    (e_fX_tmp * muDoubleScalarPower(c_a, 2.5));
  st.site = &ab_emlrtRSI;
  a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ab_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ab_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ab_emlrtRSI;
  a = (a * a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &ab_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ab_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ab_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ab_emlrtRSI;
  b_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &ab_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ab_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ab_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &ab_emlrtRSI;
  c_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fX[15] = (f_fX_tmp / muDoubleScalarPower(a, 2.5) - d_a / muDoubleScalarPower
            (b_a, 2.5)) - g_fX_tmp / (c_fX_tmp * muDoubleScalarPower(c_a, 2.5));
  st.site = &bb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &bb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &bb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &bb_emlrtRSI;
  a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &bb_emlrtRSI;
  b_a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &bb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &bb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &bb_emlrtRSI;
  b_a = (b_a * b_a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (b_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &bb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &bb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &bb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &bb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &bb_emlrtRSI;
  c_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (c_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &bb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &bb_emlrtRSI;
  d_a = (mu + X[0]) - 1.0;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &bb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &bb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &bb_emlrtRSI;
  d_a = (d_a * d_a + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (d_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &bb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &bb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &bb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &bb_emlrtRSI;
  e_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (e_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  st.site = &bb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &bb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &bb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &bb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &bb_emlrtRSI;
  f_a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (f_a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fX[25] = (((((mu - 1.0) / muDoubleScalarPower(a, 1.5) - mu /
               muDoubleScalarPower(b_a, 1.5)) - 3.0 * c_a_tmp * (mu - 1.0) /
              muDoubleScalarPower(c_a, 2.5)) + 3.0 * mu * c_a_tmp /
             muDoubleScalarPower(d_a, 2.5)) + d_fX_tmp / (c_fX_tmp *
             muDoubleScalarPower(e_a, 1.5))) - b_fX_tmp * c_a_tmp / (c_fX_tmp *
    muDoubleScalarPower(f_a, 2.5));
  fX[35] = 0.0;
  fX[45] = 0.0;
  fX[55] = 0.0;
  st.site = &cb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &cb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &cb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &cb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &cb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &cb_emlrtRSI;
  a = (a_tmp * a_tmp + b_a_tmp) + c_a_tmp;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  if (a < 0.0) {
    emlrtErrorWithMessageIdR2018a(&c_st, &emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }

  fX_tmp = -(Tmax * X[9]);
  fX[65] = fX_tmp / j_fX_tmp - d_fX_tmp * X[2] / (k_fX_tmp * muDoubleScalarPower
    (a, 1.5));
  fX[75] = 0.0;
  fX[85] = 0.0;
  fX[95] = m_fX_tmp;
  fX[6] = 0.0;
  fX[16] = 0.0;
  fX[26] = 0.0;
  fX[36] = 0.0;
  fX[46] = 0.0;
  fX[56] = 0.0;
  fX[66] = 0.0;
  st.site = &db_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &db_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &db_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &db_emlrtRSI;
  a_tmp = ((X[7] * X[7] + X[8] * X[8]) + X[9] * X[9]) + 1.0E-8;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  d_st.site = &gb_emlrtRSI;
  e_st.site = &hb_emlrtRSI;
  f_st.site = &ib_emlrtRSI;
  if (a_tmp < 0.0) {
    emlrtErrorWithMessageIdR2018a(&f_st, &b_emlrtRTEI,
      "Coder:toolbox:ElFunDomainError", "Coder:toolbox:ElFunDomainError", 3, 4,
      4, "sqrt");
  }

  fX[76] = l_fX_tmp / (c * muDoubleScalarSqrt(a_tmp));
  st.site = &eb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &eb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &eb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &eb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  d_st.site = &gb_emlrtRSI;
  e_st.site = &hb_emlrtRSI;
  f_st.site = &ib_emlrtRSI;
  if (a_tmp < 0.0) {
    emlrtErrorWithMessageIdR2018a(&f_st, &b_emlrtRTEI,
      "Coder:toolbox:ElFunDomainError", "Coder:toolbox:ElFunDomainError", 3, 4,
      4, "sqrt");
  }

  fX[86] = n_fX_tmp / (c * muDoubleScalarSqrt(a_tmp));
  st.site = &fb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &fb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &fb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  st.site = &fb_emlrtRSI;
  b_st.site = &o_emlrtRSI;
  c_st.site = &p_emlrtRSI;
  d_st.site = &gb_emlrtRSI;
  e_st.site = &hb_emlrtRSI;
  f_st.site = &ib_emlrtRSI;
  if (a_tmp < 0.0) {
    emlrtErrorWithMessageIdR2018a(&f_st, &b_emlrtRTEI,
      "Coder:toolbox:ElFunDomainError", "Coder:toolbox:ElFunDomainError", 3, 4,
      4, "sqrt");
  }

  fX[96] = fX_tmp / (c * muDoubleScalarSqrt(a_tmp));
  fX[7] = 0.0;
  fX[17] = 0.0;
  fX[27] = 0.0;
  fX[37] = 0.0;
  fX[47] = 0.0;
  fX[57] = 0.0;
  fX[67] = 0.0;
  fX[77] = 0.0;
  fX[87] = 0.0;
  fX[97] = 0.0;
  fX[8] = 0.0;
  fX[18] = 0.0;
  fX[28] = 0.0;
  fX[38] = 0.0;
  fX[48] = 0.0;
  fX[58] = 0.0;
  fX[68] = 0.0;
  fX[78] = 0.0;
  fX[88] = 0.0;
  fX[98] = 0.0;
  fX[9] = 0.0;
  fX[19] = 0.0;
  fX[29] = 0.0;
  fX[39] = 0.0;
  fX[49] = 0.0;
  fX[59] = 0.0;
  fX[69] = 0.0;
  fX[79] = 0.0;
  fX[89] = 0.0;
  fX[99] = 0.0;
}

/* End of code generation (fX_SRP.c) */
