function ll_vary = MLE_SRP(C_R_vary, SRP_traj,sigma,mu,C_R_vec, t_vec, Phi, A_sc, c)
    
    % 1D maximum likelihood estimator for SRP parameters
    
    num_points = length(SRP_traj);
    
    C_R_residuals = NaN(num_points,1);
    
    for i = 1:num_points
        X = SRP_traj(i,:)';
        t = t_vec(i);
        a_total_actual =  CR3BP_SRP_stoch(t,X,mu,C_R_vec,t_vec,Phi,A_sc,c);
        Xdot_CR3BP = CR3BP(t,X(1:6),mu);
        a_CR3BP = Xdot_CR3BP(4:6);
        a_SRP_actual = a_total_actual(4:6) - a_CR3BP;
        R_sc = X(1:3);
        R_sun = [-mu; 0; 0]; % Assuming Sun is first primary, DOES NOT WORK OTHERWISE
        R2 = R_sc - R_sun;
        r2 = norm(R2);
        C_R_actual = c*r2^2/Phi*X(7)/A_sc*norm(a_SRP_actual);
        C_R_residuals(i) = C_R_actual;
    end
    
    % Compute log likelihood
    
    ll_vary = NaN(length(C_R_vary),1);
    
    for cr = 1:length(C_R_vary)
        C_R_hat = C_R_vary(cr);
        l = 0;
        for i = 1:num_points
            %L_i = log(1/sqrt(2*pi*sigma^2)*exp(-1/2*(C_R_residual_vec(i) - C_R_hat)^2/sigma^2));
            L_i = log(1/sqrt(2*pi*sigma^2)) + (-1/2*(C_R_residuals(i) - C_R_hat)^2/sigma^2);
            l = l + L_i;
        end
        ll_vary(cr) = l;
    end

end